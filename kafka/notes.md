# Kafka intro

## Definitions

`Topic` - All messages in Kafka are stored and retrieved from Topics. These topics are, in turn, stored in Brokers. The actual logs that keep the immutable messages in Topics are Partitions

`Partition` - All messages in Kafka are stored and retrieved from Topics. These topics are, in turn, stored in Brokers. The actual logs that keep the immutable messages in Topics are Partitions

`Cluster` - A group of brokers form a cluster

`Broker` - A server that is available in the Kafka cluster and hosts the topics

`Zookeeper` - Α centralized service that is used in a number of projects, including Kafka and Hadoop. In Kafka it is mainly used for monitoring cluster membership metadata, managing configuration of topics and fault detection

`Producer` - Independent applications that generate messages and connect to the cluster to publish them. Messages sent from producers are stored in the Kafka Topics

`Consumer` - Independent applications that subscribe to Kafka Topics and partitions and retrieve messages stored, for processing

`Producer Record` - 'Messages' in Kafka terms

`Consumer Group` - Consumers can form 'consumer groups', that allow them to retrieveand process messages as if they were one app.

`Message Offset` - Message offsets are unique 'IDs'  that messages have when stored in a partition. These offsets are also used by consumers to keep track of the messages they have already read and continue processing from that point on

## Topics to cover again

* 25, 26, 27 - The process of sending messages
* 37, 38, 40 - Polling loop

## Installation

See the Coach video for installation. It is install in:

```bash
    cd /usr/local/bin/kafka
```

## Starting Kafka

1. start zookeeper - the main server

    ```bash
    bin/zookeeper-server-start.sh config/zookeeper.properties
    telnet localhost 2181 # connect
    stat # gives the status of the node
    ```

2. start a broker

    ```bash
    bin/kafka-server-start.sh config/server.properties
    ```

3. create a topic

    ```bash
    bin/kafka-topics.sh --create --topic my_topic --zookeeper localhost:2181 --replication-factor 1 --partitions 1
    # give in the zookeeper as it assign the topic to the broker
    ```

    The partions are phisical represations of the commit logs stored on one or more brokers. Each partition must fit on signle machine.
    You can't split a partion over two machines. If you use:

    ```bash
        bin/kafka-topics.sh --create --topic my_topic
        --zookeeper localhost:2181
        --replication-factor 1
        --partitions 3
    ```

    * `replication-factor 1` means that every topic's partition will have only single replica, meaning not replicaiton for backup.
    * `replication-factor n` guarantees n-1 broker failures. Min should be 2 or 3 for a cluster.
    * Replication factor is set at the topic's level.

    We cause single topic to be split across 3 different log files. This should have 3 different broker nodes.

    Test this:

    ```bash
        bin/kafka-topics.sh --create --topic my_topic
        --zookeeper localhost:2181
        --replication-factor 3
        --partitions 3
    ```

    To list available topics.

    ```bash
        bin/kafka-topics.sh --list --zookeeper localhost:2181
    ```

    To see what is going on with all partitions, replicas, and the `isr` use the below.
    When `irs` is equal to the replication factor, the broker node is in a healthy state.

    ```bash
        bin/kafka-topics.sh --describe --topic my_topic --zookeeper localhost:2181
    ```

4. create a producer

    ```bash
        bin/kafka-console-producer.sh --broker-list localhost:9092 --topic my_topic
        # the port is the one produce pushes things to
        # this is input message service, type your messages in
        # >test1
        # >test2
    ```

5. create a consumer

    ```bash
        bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my_topic --from-beginning
        # the port works as the same as producer
    ```

6. check messages in the log

    ```bash
        cd /tmp/kafka-logs/my_topic-0
        cat *.log # should see the message if they were received
    ```

## Example

Example with 3 brokers. You need to set up port and log folder for each broker.

```bash
    cd /usr/local/bin/kafka
    bin/zookeeper-server-start.sh config/zookeeper.properties
    bin/kafka-server-start.sh config/server-0.properties
    bin/kafka-server-start.sh config/server-1.properties
    bin/kafka-server-start.sh config/server-2.properties
```

```bash
    bin/kafka-topics.sh --create --topic my-topic --zookeeper localhost:2181 --replication-factor 3 --partitions 3
    bin/kafka-topics.sh --create --topic my-other-topic --zookeeper localhost:2181 --replication-factor 3 --partitions 3
    bin/kafka-topics.sh --delete --topic my-topic --zookeeper localhost:2181
    bin/kafka-topics.sh --alter --topic my-topic --zookeeper localhost:2181 --partitions 4
    bin/kafka-topics.sh --list --zookeeper localhost:2181
    bin/kafka-topics.sh --describe --zookeeper localhost:2181 # --topic my-topic  # if not topic given, describe all
    >>
    Topic:replicated_topic  PartitionCount:3        ReplicationFactor:3     Configs:
        Topic: replicated_topic Partition: 0    Leader: 1       Replicas: 1,0,2 Isr: 1,0,2
        Topic: replicated_topic Partition: 1    Leader: 2       Replicas: 2,1,0 Isr: 2,1,0
        Topic: replicated_topic Partition: 2    Leader: 0       Replicas: 0,2,1 Isr: 0,2,1
```

We have 3 partitions. 3 replicas in on each broker. Isr (In-sync replicas) in the same order. The leader is the first in the list of replicas.

```bash
    bin/kafka-console-producer.sh --broker-list localhost:9092 --topic my-topic
    bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my-topic --from-beginning
```

Establish basic producer. This is the test producer

```bash
    bin/kafka-producer-perf-test.sh --topic my_topic-topic --num-records 50 --record-size 1 --throughput 10 --producer-props bootstrap.servers=localhost:9092 key.serializer=org.apache.kafka.common.serialization.StringSerializer value.serializer=org.apache.kafka.common.serialization.StringSerializer
```

* `num-records` - 50 records
* `record-size` - 1 bite each
* `throughput` - 10 per second
* the rest is required properties

## Writing a producer

It is Java based. Settings to look at in micro batching:

* `batch.size` - number of bites within batch within local buffer
* `buffer.memory` - number of bites the buffer can take
* `max.block.ms` - how many miliseconds the send method will be blocked for when you reach buffer memmory limit

Delivery Guarantees:

* `acks=0` - fire and forget, no acknowledgement is send back by the broker, fastest but not reliable
* `acks=1` - leader acknowledged, only the leader broker to confirm, good balance of rerpormance and reliability
* `acks=0` - all in sync replicas sends recepts, slower performance

If error is send back,

* `retries` - how many times the producer will try to send the message
* `retry.backoff.ms` - no of miliseconds between re-tries to send
* `max.in.flight.request.per.connection` - number of requests that can be made by the producer

## Writing a consumer

* `polling()` - method - collect meta data about the cluster
* `polling(1000)` - timeout setting, number of milliseconds the consumer is to spend polling/collecting data from the cluster; minimum amount of time each message retrieval cycle will take
* `enable.auto.commit=true` - Offset should be set manually (set this to false) after processing the message, not when receiving the message. Read is not the same as commited. `enable.auto.commit=false` commit when you see the message as processed.
* `auto.commit.interval.ms=5000` - every 5 seconds a consumer is going to commit its offset if `enable.auto.commit=true`. If `enable.auto.commit=false`, this is ignored.
* `auto.offset.rest= "latest";"earliest";"none"` - strategy to use when reader starts from a new partition - `latest` known commited offset, `earlist` known commited offset, `none` throw exeption to the consumer and decide what to do with it
* `myConsumer.commitSync()` - method, use this when you consider record truelly processed, higher consistency is required, after a batch of messages, this will hold the thread until it receives confirmation, retries until suceeds or unrecoverable error - `retry.backoff.ms=100` - retry in 0.1s to recover
* `myConsumer.commitAsunc()` - non-blocking but non-deterministic, it doesn't retry automatically. Can lead to order issues and duplication of records. `Callback` option

Kafka stores the offsets is a specific topic: `__consumer_offset`.

### Consumer Groups

* `group.id` - set up this with a group, it does the rebalancing if a consumer if it fails or a new one joins.
* `heartbeat.interval.ms=3000` - GroupCoordinator in the broker cluster relies on this to make sure that the individual consumers are running
* `session.timeout.ms=30000` - GroupCoordinator waits for this time before it takes corective action
* `fetch.min.bytes` - minimum number of bytes that must be returned from the pool, like batch size size setting on producer
* `max.fetch.wait.ms` - like linger.ms setting in producer, wait for this time to get the bytes from min bytes setting
* `max.partition.fetch.bytes` - set this up to limit max bytes being fetched per partiion to ensure that your consumer can keep up per cycle
* `max.poll.records` - max number of records allowed per poll cycle, to slow down the number of incoming messages

#### Consumer Position Control

Consumer position control api, define the offset?

* `seek()` method - specifi given offeset in a given topic to start reading from
* `seekToBeginning()` method - you wanna start from the beginning of a given topic and partition
* `seekToEnd()` - method - start consuming from the end of the queue

#### Flow Control

Pause or resume consuming the messages

* `pausa` - method - which topics and partitions you wanna pause;
* `resume` - method - and resume

Use this when single consumer reads from mulitple partitions/topics

#### Rebalance Listener

Notify you when rebalance event occures so you can manage the rebalance

## Other projects

* `Schema Registry`
* `Kafka Connect and Hub`
* `Kafka Streams`

### Other commands

```bash
    # needs atleast single broker running
    cd /usr/local/bin/kafka
    bin/kafka-topics.sh --zookeeper local:2181 --describe --topic
    bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic __consumer_offsets # this doesn't work for me
```

## Editing Json messages
use jq app in Linux to edit the messages.

```bash
    jq .  kafka_message.json > testing.json
```