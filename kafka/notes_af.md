# Openbet Kafka in Docker

Kafka setup files are in:

```bash
    /opt/openbet/current/activity_feeds_v2/kafka/
```

List all topics:

```bash
    bin/kafka-topics.sh --list --zookeeper localhost:19374
```

To have a current build in the docker, delete the `activity_feed_v2` folder and replace it with

```bash
rad@base:/space/sportsbook/activity-feeds-v2/distribution/target (master)$ ls
archive-tmp  dependency-maven-plugin-markers  willhill-activity-feeds-distribution-1.44-SNAPSHOT.jar
dependency   maven-archiver                   willhill-activity-feeds-distribution-1.44-SNAPSHOT.tar.gz
```

the tar file.

Read the message
```bash
     bin/kafka-console-consumer.sh --bootstrap-server localhost:19374 --topic bets --from-beginning
```
or this

```bash
    /opt/openbet/current/activity_feeds_v2/kafka_reader/bin/run.sh --groupid 1 --topic betslips
```