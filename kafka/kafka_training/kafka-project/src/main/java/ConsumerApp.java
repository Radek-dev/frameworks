import org.apache.kafka.common.*;
import org.apache.kafka.clients.consumer.*;

import java.util.*;


/*
 *
 *   This app subscribes to the bets_topic and polls for messages
 *   Multiple instances of this app can be run.
 *
 *
 * */

public class ConsumerApp {
    public static void main(String[] args) {

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092, localhost:9093, localhost:9094");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "BetPlacementDeserializer");
        props.put("group.id", "my-group");
        props.put("auto.offset.reset", "latest");
   /*     props.put("fetch.min.bytes", 1);
        props.put("zookeeper.connect", "localhost:2181");
        props.put("heartbeat.interval.ms", 3000);
        props.put("max.partition.fetch.bytes", 1048576);
        props.put("session.timeout.ms", 30000);

        props.put("connections.max.idle.ms", 540000);
        props.put("enable.auto.commit", true);
        props.put("exclude.internal.topics", true);
        props.put("max.poll.records", 2147483647);
        props.put("partition.assignment.strategy", "org.apache.kafka.clients.consumer.RangeAssignor");
        props.put("request.timeout.ms", 40000);
        props.put("auto.commit.interval.ms", 5000);
        props.put("fetch.max.wait.ms", 500);
        props.put("metadata.max.age.ms", 300000);
        props.put("reconnect.backoff.ms", 50);
        props.put("retry.backoff.ms", 100);
        props.put("client.id", "");*/


        KafkaConsumer<String, BetPlacement> myConsumer = new KafkaConsumer<>(props);

        ArrayList<String> topics = new ArrayList<String>();
        topics.add("bets_topic");
        myConsumer.subscribe(topics);


        Set<String> subscribedTopics = myConsumer.subscription();

        printSet(subscribedTopics);

        try {
            while (true) {
                ConsumerRecords<String, BetPlacement> records = myConsumer.poll(1000);
                for (ConsumerRecord<String, BetPlacement> message : records) {
                    System.out.println("Message received " + message.value().toString());
                    //printRecords(records);
                }
            }
        } finally {
            myConsumer.close();
        }

    }

    private static void printSet(Set<String> collection){
        if (collection.isEmpty()) {
            System.out.println("I am not subscribed to anything yet...");
        }
        else {
            System.out.println("I am subscribed to the following topics:");
            for (String item : collection){
                System.out.println(item);
            }
        }
    }
}

