import org.apache.kafka.common.*;
import org.apache.kafka.clients.consumer.*;

import java.util.*;

/*
 *
 *   This app ets assigned to the access_topic - Partition0 and polls for messages
 *
 *
 * */

public class ConsumerAssignApp {
    public static void main(String[] args){

        Properties props = new Properties();

        props.put("bootstrap.servers", "localhost:9092, localhost:9093, localhost:9094");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "WebsiteDeserializer");
		props.put("group.id", "");
        props.put("auto.offset.reset", "latest");
       /* props.put("fetch.min.bytes", 1);
        props.put("heartbeat.interval.ms", 3000);
        props.put("max.partition.fetch.bytes", 1048576);
        props.put("session.timeout.ms", 30000);
        props.put("auto.offset.reset", "latest");
        props.put("connections.max.idle.ms", 540000);
        props.put("enable.auto.commit", true);
        props.put("exclude.internal.topics", true);
        props.put("max.poll.records", 2147483647);
        props.put("partition.assignment.strategy", "org.apache.kafka.clients.consumer.RangeAssignor");
        props.put("request.timeout.ms", 40000);
        props.put("auto.commit.interval.ms", 5000);
        props.put("fetch.max.wait.ms", 500);
        props.put("metadata.max.age.ms", 300000);
        props.put("reconnect.backoff.ms", 50);
        props.put("retry.backoff.ms", 100);
        props.put("client.id", "");
*/

        KafkaConsumer<String, WebsiteAccess> myConsumer = new KafkaConsumer<>(props);

        ArrayList<TopicPartition> partitions = new ArrayList<TopicPartition>();
        partitions.add(new TopicPartition("access_topic", 0 )); // Adds an additional TopicPartition instance representing a different partition within the topic. Change as desired.
        myConsumer.assign(partitions);

        Set<TopicPartition> assignedPartitions = myConsumer.assignment();

        printSet(assignedPartitions);

        try {
            while (true){
                ConsumerRecords<String, WebsiteAccess> records = myConsumer.poll(1000);
                for (ConsumerRecord<String, WebsiteAccess> message : records) {
                    System.out.println("Message received " + message.value().toString());

                }
            }
        } finally {
            myConsumer.close();
        }

    }

    private static void printSet(Set<TopicPartition> collection){
        if (collection.isEmpty()) {
            System.out.println("I do not have any partitions assigned yet...");
        }
        else {
            System.out.println("I am assigned to following partitions:");
            for (TopicPartition partition: collection){
                System.out.println(String.format("Partition: %s in Topic: %s", Integer.toString(partition.partition()), partition.topic()));
            }
        }
    }

}
