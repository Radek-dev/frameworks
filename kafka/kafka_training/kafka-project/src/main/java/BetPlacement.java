import java.io.Serializable;

public class BetPlacement implements Serializable {

    private String betType;
    private int custid;
    private Double stake;

    public String getBetType() {
        return betType;
    }

    public int custid() { return custid; }

    public void setBetType(String betType) {
        this.betType = betType;
    }

    public void setCustId(int custid) { this.custid = custid; }

    public Double getStake() {
        return stake;
    }

    public void setStake(Double stake) {
        this.stake = stake;
    }

    @Override
    public String toString() {
        return "BetPlacement{" +
                "betType='" + betType + '\'' +
                ", custid=" + custid +
                ", stake=" + stake +
                '}';
    }
}
