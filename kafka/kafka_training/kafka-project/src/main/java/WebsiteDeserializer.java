import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;

public class WebsiteDeserializer implements org.apache.kafka.common.serialization.Deserializer<WebsiteAccess> {


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public WebsiteAccess deserialize(String topic, byte[] data) {

        WebsiteAccess WebsiteAccess = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(data); ObjectInputStream objectInputStream = new ObjectInputStream(bis)) {
            WebsiteAccess = (WebsiteAccess) objectInputStream.readObject();
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return WebsiteAccess;
    }

    @Override
    public void close() {

    }
}
