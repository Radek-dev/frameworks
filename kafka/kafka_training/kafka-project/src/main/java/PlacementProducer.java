import org.apache.kafka.clients.producer.*;

import java.text.*;
import java.util.*;


/*
 *
 *   This app send a predefined number of messages to the bets_topic in the cluster
 *   Messages are either login or logout messages. A simple modulo defines the type.
 *
 *
 * */

public class PlacementProducer {


    public static void main(String[] args){

        Properties props = new Properties();

        props.put("bootstrap.servers", "localhost:9092, localhost:9093, localhost:9094");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "BetPlacementSerializer");
        props.put("acks", "0");
       /* props.put("buffer.memory", 33554432);
        props.put("compression.type", "none");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("client.id", "");
        props.put("linger.ms", 0);
        props.put("max.block.ms", 60000);
        props.put("max.request.size", 1048576);
        props.put("partitioner.class", "org.apache.kafka.clients.producer.internals.DefaultPartitioner");
        props.put("request.timeout.ms", 30000);
        props.put("timeout.ms", 30000);
        props.put("max.in.flight.requests.per.connection", 5);
        props.put("retry.backoff.ms", 5);
*/
        KafkaProducer<String, BetPlacement> myProducer = new KafkaProducer<>(props);
        BetPlacement message = new BetPlacement();
        Random rand = new Random();
        int stake;
        int custid;
        String topic = "bets_topic";

        // number of records to send
        int numberOfRecords = 300;
        stake = rand.nextInt(500)+1;

        try {
            for (int i = 0; i < numberOfRecords; i++ ) {
                stake = rand.nextInt(500) + 1;
                custid = rand.nextInt(500) + 1;

                message.setStake(stake);
                message.setCustId(custid);

                myProducer.send(new ProducerRecord<String, BetPlacement>(topic, message));
                System.out.println("Message " + message.toString() + " sent !!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            myProducer.close();
        }

    }
}
