import java.io.Serializable;

public class WebsiteAccess implements Serializable {

    private String accesstype;
    private int custid;

    public String accesstype() {
        return accesstype;
    }

    public int custid() { return custid; }

    public void setaccesstype(String accesstype) {
        this.accesstype = accesstype;
    }

    public void setCustId(int custid) { this.custid = custid; }

   @Override
    public String toString() {
        return "Access{" +
                "Accesstype='" + accesstype + '\'' +
                ", custid=" + custid +
                '}';
    }
}
