import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Map;

public class WebsiteSerializer implements org.apache.kafka.common.serialization.Serializer<WebsiteAccess> {


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, WebsiteAccess data) {

        byte[] bytes = null;

        if (data != null) {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
                out.writeObject(data);
                out.flush();
                bytes = bos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return bytes;
    }

    @Override
    public void close() {

    }

}
