import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;

public class BetPlacementDeserializer implements org.apache.kafka.common.serialization.Deserializer<BetPlacement> {


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public BetPlacement deserialize(String topic, byte[] data) {

        BetPlacement betPlacement = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(data); ObjectInputStream objectInputStream = new ObjectInputStream(bis)) {
            betPlacement = (BetPlacement) objectInputStream.readObject();
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return betPlacement;	
    }

    @Override
    public void close() {

    }
}
