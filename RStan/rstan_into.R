## INSTALLATION ###
# https://github.com/stan-dev/rstan/wiki/Installing-RStan-on-Linux
# https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started

remove.packages("rstan")
if (file.exists(".RData")) file.remove(".RData")

# Add Michael Rutter's c2d4u3.5 PPA (and rrutter3.5 for CRAN builds too)
sudo add-apt-repository -y "ppa:marutter/rrutter3.5"
sudo add-apt-repository -y "ppa:marutter/c2d4u3.5"
sudo apt update
sudo apt install r-cran-rstan


# specify the number of core
Sys.setenv(MAKEFLAG = "-j8")

install.packages('rstan',
                 repot='https://cloud.r-project.org/',
                 dependencies = TRUE)

# code .stan files

# C++ toolchain configuration
dotR <- file.path(Sys.getenv("HOME"), ".R")
if (!file.exists(dotR)) dir.create(dotR)
M <- file.path(dotR, "Makevars")
if (!file.exists(M)) file.create(M)
cat("\nCXX14FLAGS=-O3 -march=native -mtune=native -fPIC",
    "CXX14=g++", # or clang++ but you may need a version postfix
    file = M, sep = "\n", append = TRUE)

# Example in Stan
library("rstan") 
options(mc.cores = parallel::detectCores())


