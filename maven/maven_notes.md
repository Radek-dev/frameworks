# Maven Notes

## Maven Structure
defaults:

* src/main/java - source directory
	- store all jave code
	- the beginning of our packge declaration
	- com.yourcampanyname.division
	- other languguage: src/main/groovy

* src/test/jave - test directory - unit tests

* target - compiled code
	- builds
	- unit tests run from here

* POM.xml - instructions
	- can be devided into 4 basic parts:
		-- project information: groupId, artifactId, version7, packaging
		-- dependencies: direct dependencies used in our application
		-- build: plugins, directory structure (over write src/main/jave structure)
		-- repositories: where to get the artifacts from

## Plugins

### Compiler Plugin
Used to compile code and test code
Invokes Javac, but with the classpath set from the dependencies
Defaults to Java 1.5 regardless of what JDK is installed (so overridden)
allows for customization

```xml
	<plugin>
		<groupId>org.apache.maven.plugins</groupId>
		<artifactId>maven-compiler-plugin</artifactId>
		<version>2.3.1</version>
		<configuration>
			<!--make you own fork-->
			<fork>true</fork>
			<!--min and max memory to use-->
			<meminitial>128m</meminitial>
			<maxmen>512m</maxmen>
			<!--different target of what we are compiling-->
			<source>1.7</source>
			<target>1.7</target>
		</configuration>
	</plugin>
```

## Jar Plugin
Used to package code into a jar
Tied to the package phase
Configuration:
	Inlcludes/Exludes
	Manifest

## Source Plugin
Used to attach source code to a jar
Tied to the package phase
	Ofetr overridden to a later phase

## Java Doc Plugin
Used to attach Javadocs to a jar file
Tied to the package phase
	Often overridden to a later phase
Usually just use the defaults, but many customization options for Javadoc format


## Goals
Goals are compile, clean or package, test, package, install, deploy
Super pom carries the defaults

Goals are really just configured plugins in your application
Plugins are tied to one of the defined phases, but can usually be overridden


```xml
	<plugin>
		<artifactI>maven-clean-plugin</artifactI>
		<version>2.4.1</version>
		<executions>
			<executions>
				<id>default-clean</id>
				<phase>clean</phase>
				<goals>
					<goal>clean</goal>
				</goals>
			</executions>
		</executions>
	</plugin>
```
goal - clean runs inteh clean phase

## Phases
* validate - all info available for the project
* compile - compile the project source code
* test - test the compiled source code
* package - packages the code in its defined packge, such as a JAR
* integration-test - deploy and run integration tests
* verify - run checks against package to verify integrity
* install - install the package in our local repo
* deploy - copy final package to a remote repository

## Dependencies
- what we want to use in our application
- imported by their naming convention (artifactID, groupID, version)
	-- this may be confusing
- handles transitive dependencies for us

this is what it looks like

```xml
	<dependencies>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.1</version>
		</dependency>
	</dependencies>
```
Add it at the end of the pom file.

groupId...as our company, it may not be that
artifactId...actual set of objects that we want to use
versions...version of the dependency

### SNAPSHOT
This means get me the latest code. It checkes for changes.
But this is dynamic, so we may not know if it will work.
Example version: myapp-1.0-SNAPSHOT
	-- the version is 10.0-SNAPSHOT, myapp - artifactId

Saves us from rereleasing versions for development.
Never deply to production with a SNAPSHOT

### Packaging Types
Current core packing types: pom, jar, war, ear, rar, par, pom
pom: maven gets the dependecies for this
<packaging>jar</packaging> - example

### Transitive Dependencies
If you wanna get a single artifact, maven get all dependencies for that artifact.
This may be confusing, as the user of the single artifact doesn't know the dependencies.
It resolves conflicts too.

### Scope
There are 6 scopes available for dependencies:
* compile - default scope, artifacts available everywhere
	- all resources are available everywhere
* provided - means that the artificat is going to be provided where it is deployed
	- available in all phases, but not included in final artifact
* runtime - not needed for compilation, but needed for execution
	- dynamically loaded
	- not included in final artifact
* test - only for the test compilation and execution phase
* system - similar to provided, hardcoded path to a jar in the system
	- don't use
* import - deals with dependencyManagement sections



## Repositoris
http accessible location that you download files from
Super pom.xml - default with Maven installtion
	-- default location is http://repo.manve.apache.org/maven2 (this has all the open source projects)
	-- this has 95% that we need to download
Multiple reposotories are fine
Corporate repositories:
	-- Nexus
	-- Artifactory

### Local Repo
Maven stores everything locally, first. If not available there, it goes to download.
Store artifacts using the groupId, artifactId, version
-- i.e.: /home/<your_username>/.m2/repository/commons-lang/commons-lang/2.1/commons-lang-2.1.jar

this is to avoid duplication by copying it in every project and storing it in your SCM
defauls is: /home/<your_username>/.m2/repository
my settings is: /space/pipeline

the full path is: /<your_repo_path>/artifactId/groupId/version/<artifactId>-<version>.<packiging extension>
	/space/pipelien/basic_project/

### Dependency Repo
where we get our dependencies from
may be multiple repos like this:

```xml
	<profiles>
		<profile>
			<id>default</id>
			<repositories>
				<repository>
					<id>remoteRepository</id>
					<name>My Remote Repository</name>
					<url>dav:https://developer.us2.oraclecloud.com/……/maven/</url>
					<layout>default</layout>
				<snapshots>
					<enabled>true</enabled>
				</snapshots>
				<releases>
					<anabled>false</enabled>
				</releases>
				</repository>
			</repositories>
		</profile>
	</profiles>
```

must have id, we wanna give it name and the url to the repo

### Plugin Repo
### Release/Snapshots Repos
