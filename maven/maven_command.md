# Maven Commands

## Basics

```bash
	maven clean     # set up the structure of the project, cleans up
	maven compile   # this is the compile the java(/any) application
	maven package   # generate the package as defined by <packaging> token, this runs the test too
	maven install   # run the package command and then install it in your local repo
			# 	my local repository is set to /space/pipeline - all builds are there
	maven deploy    # runs the install command and deploys it to a corporate repository
```

## Get effective pom

```bash
	mvn help:effective-pom
```