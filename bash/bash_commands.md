# Bash

## Useful commands

Get folder structure without tree
```bash
ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'
ls -LR
find -follow
```

