# C++ Key Ideas

Variables memory allocation  varies with a compiler
* so int may be 2 bite or 4 bite depending on a compiler
* c++ may run differently on different OS

Variables have garbage data in them before the you assign any value to them

## Pointers

* Pointer operators & and *
* Pass-by-value vs pass-by-reference with pointers
* Applying const to pointer variables
* The sizeof operators
* Referencing array elements with array name and pointers
* Pointers to functions
* Array of pointers to functions

reference operator:
`'&' means reference of a value` 

dereference operator
`\'*' means value of a reference,` 

'&*' is the same, does both and so nothing

## Inheritance
Contractors and Distractors are not inherited
### Public Inheritance
Public member will become public in the derived class.
Public parent's attributes will be visible in the child, not the private ones.

* `Public members` of a class A are accessible for all and everyone.
* `Protected members` of a class A are not accessible outside of A's code, but is accessible from the code of any class derived from A.
* `Private members` of a class A are not accessible outside of A's code, or from the code of any class derived from A.

## Greg's Advice
I mentioned C++ 11 because this extension of C++ and the STL involved important changes and some old textbooks don’t cover them.
[here](https://smartbear.com/blog/develop/the-biggest-changes-in-c11-and-why-you-should-care/)

This depends on the version of your compiler, you will definitely have them in any recent version of Visual Studio or gcc for example.

By the way this page covers some of the tricky parts of cpp.
[here](https://isocpp.org/faq)

I would suggest you consult the official reference when looking up something (although it can be a bit more difficult).
[here](https://en.cppreference.com/w/)

## Things to go over again
* pointers
* friends
* case studies
    * case study: Payroll system using polymorphism
    