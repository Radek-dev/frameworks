#include <iostream>

// to compile:
// g++ -o name source_fill.cpp
// g++ -o test main.cpp

// compile all cpp files
// g++ *.cpp

int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
