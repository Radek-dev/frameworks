-- Use dbshema to see the details about the table
-- 'openbet' is the name of the database
-- tcntrybancat is a table
```bash
dbschema -d openbet -t tcntrybancat;
```

-- use this to see the procedure
```bash
dbschema -d openbet -f pinsafgenericqueue;
```

-- or
```bash
dbschema -d openbet -t tbet
dbschema -d openbet -t tXGameBet
```

-- print out table from the db
```bash
PREPROD 1[openbet@pp1ux510.willhill:~]$ dbcmd --db $INFORMIXDATABASE sql " select * from tkyccust_aud where cust_id=2033;" 
```