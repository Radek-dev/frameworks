
-- proc with temp tample
https://stash.int.openbet.com/projects/WIL/repos/release-database-scripts/pull-requests/727/diff


-- Static data changes for all live and test environments and for both offshore and onshore

set lock mode to wait 60;

!echo "`date '+%Y-%m-%d %H:%M:%S'`: WIL-XXXXX - Project Title - Begin"
!echo "`date '+%Y-%m-%d %H:%M:%S'`: WIL-62595 - Spitfire - Spain Test Account preparation - Begin"

!echo "`date '+%Y-%m-%d %H:%M:%S'`: WIL-XXXXX - Project Title - End"
    -- drop temp table if somehow it exists
    drop table if exists tEspTestAccounts_tmp;

    -- create temporary table to hold cust IDs, password and salt of test accounts
    create temp table tEspTestAccounts_tmp(
        cust_id integer,
        password varchar(100),
        password_salt varchar(100),
        balance decimal
    );

    -- drop procedure if it has persisted
    drop procedure if exists pInsEspTestAccountUpdates();

    -- create procedure to do the following:
    -- 1. Create temp table to store all spain test account cust IDs with usernames starting with "NFTTESTES*"
    -- 2. Unload a CSV containing the passwords & password salts for rollback
    -- 2. Update those accounts balance to £1000.00
    -- 3. Add the TestAccount flag to all the accounts
    -- 4. Update all their passwords to the same thing
        --##################################################
        --password='T3stTeam_2o16' alg='sha1'
        --case sensitive: 1
        --salt = 'b852a73b7b834011'
        --enc_pwd(SHA1) = '272a10f5f9c05a2f0584b561e68026e283018cef'
        --##################################################

    create procedure pInsEspTestAccountUpdates()

        -- define proc variables
        define v_cust_id like tCustomer.cust_id;
        define v_password like tCustomer.password;
        define v_pass_salt like tCustomer.password_salt;
        define v_balance like tAcct.balance;

        define work_count int;
        define batch_size int;
        let work_count = 0;
        let batch_size = 100;

        -- begin to loop through accounts, perform actions for each
        foreach with hold
            select 
                c.cust_id,
                c.password,
                c.password_salt,
                a.balance
            into
                v_cust_id,
                v_password,
                v_pass_salt,
                v_balance
            from
                tCustomer c,
                tAcct a
            where 
                c.cust_id = a.cust_id
                and c.username like "NFTTESTES%"

            begin work;

            insert into 
                tEspTestAccounts_tmp(
                    cust_id, 
                    password, 
                    password_salt,
                    balance)
            values (
                v_cust_id,
                v_password,
                v_pass_salt,
                v_balance
            );

            -- update balance
            update 
                tAcct 
            set
                balance = 1000.00,
                status = "A"
            where
                cust_id = v_cust_id;

            -- check for existing flag
            if not exists (
                select 1
                from
                    tCustomerFlag
                where
                    cust_id = v_cust_id
                    and flag_name = "TestAccount"
            ) then
                -- insert TestAccount flag
                insert into 
                    tcustomerflag
                values (
                    v_cust_id,  
                    "TestAccount", 
                    "Y", 
                    "CUST_INET"
                );
            else
                -- ensure existing flag has the same values as the newly inserted ones
                update
                    tCustomerFlag
                set
                    flag_value = "Y",
                    owner = "CUST_INET"
                where
                    cust_id = v_cust_id
                    and flag_name = "TestAccount";
            end if

            -- update passwords
            update 
                tCustomer 
            set (
                password, 
                password_salt) = (
                '272a10f5f9c05a2f0584b561e68026e283018cef', 
                'b852a73b7b834011'
            ) 
            where 
                cust_id = v_cust_id;

            commit work;

            -- check how many records are updated, sleep after every 100 processed
            -- 7173 records, so script will take 3.5 mins approx with a 3 second sleep
            let work_count = work_count + 1;
            if work_count = batch_size then
                let batch_size = batch_size + 100;
                SYSTEM "sleep 3";
            end if

        end foreach

    end procedure;

    -- run procedure above
    execute procedure pInsEspTestAccountUpdates();

    --  place passwords and salts into csv in case we need to rollback
    unload to /openbet_store/users/smolinar/wil_62595.csv select cust_id, password, password_salt, balance from tEspTestAccounts_tmp;

    -- delete procedure as its a one-run-and-done
    drop procedure pInsEspTestAccountUpdates();

    -- drop temp table as we no longer need its values
    drop table tEspTestAccounts_tmp

!echo "`date '+%Y-%m-%d %H:%M:%S'`: WIL-62595 - Spitfire - Spain Test Account preparation - End"