/*
all tables and columns
SELECT TRIM(t.tabname) || '.' || TRIM(c.colname) AS table_dot_column
  FROM "informix".systables AS t, "informix".syscolumns AS c
 WHERE t.tabid = c.tabid
   AND t.tabtype = 'T'
   AND t.tabid >= 100
 ORDER BY t.tabname, c.colno;

select
    c.cust_id,
    c.username,
    c.password,
    r.fname,
    r.lname,
    c.bet_count
from
    tcustomer c,
    tcustomerreg r
where
    c.cust_id = r.cust_id
    and c.cust_id = 888;

select
    b.receipt
from
    tcustomer c,
    tacct a,
    tbet b
where
    c.cust_id = a.cust_id
    and a.acct_id = b.acct_id
    and c.cust_id = 100;



select
    e.desc
from 
    tevoc e,
    tobet o,
    tbet b
where
    o.ev_oc_id = e.ev_oc_id
    and o.bet_id = b.bet_id
    and b.bet_id = 237725
;

select
    b.receipt,
    b.bet_id
from
    tcustomer c,
    tacct a,
    tbet b
where
    c.cust_id = a.cust_id
    and a.acct_id = b.acct_id
    and c.cust_id = 100
    ;



The data to be displayed for each level are:
Categories: The name column in the tevcategory table
Classes: The name column of the tevclass table
Types: The name column of the tevtype table
Events: The desc column of the tev table
Markets: The name column of the tevmkt table
Selections: The desc column of the tevoc 
*/

/*
select
    ctg.name as category,
    cl.name as class,
    tp.name as type,
    ev.desc as event,
    mkt.name as market,
    oc.desc as selectin
from
    tevcategory ctg,
    tevclass cl,
    tevtype tp,
    tev ev,
    tevmkt mkt,
    tevoc oc
where
    ctg.category = cl.category and 
    cl.ev_class_id = tp.ev_class_id and
    tp.ev_type_id = ev.ev_type_id and
    ev.ev_id = mkt.ev_id and
    ev.ev_id = oc.ev_id and
    mkt.ev_mkt_id = oc.ev_mkt_id
;

select
    column_name
from
    upper_table ut,
    lower_table lt
where
    fk_line
    ut.filter_value
;
*/

/*
select
    ctg.name as category
from 
    tevcategory ctg
;

select
    cl.name as class
from
    tevcategory ctg,
    tevclass cl
where
    ctg.category = cl.category
    and ctg.name = "|American Football|"
;

select
    tp.name as output
from
    tevclass cl,
    tevtype tp
where
    cl.ev_class_id = tp.ev_class_id
    and cl.name = "|American Football Specials|"
;

select
    ev.desc as output
from
    tevtype tp,
    tev ev
where
    tp.ev_type_id = ev.ev_type_id
    and tp.name = ""
;

select
    mkt.name as output
from
    tev ev,
    tevmkt mkt
where
    ev.ev_id = mkt.ev_id
    and ev.desc = ""
;

select
    oc.desc as output
from
    tevmkt mkt,
    tevoc oc
where
    mkt.ev_mkt_id = oc.ev_mkt_id
    and mkt.name = ""
;
*/

--winning exercise - appserver


-- select  first 10 
--     c.cust_id,
--     c.username,
--     c.status,
--     c.sort,
--     c.acct_no,
--     c.bet_count - (select count(*) from tbet where acct_id=a.acct_id and status='X') as bet_count,
--     c.country_code,
--     c.elite,
--     a.ccy_code,
--     c.cr_date,
--     c.max_stake_scale,
--     a.balance + a.sum_ap AS balance,
--     a.credit_limit,
--     r.nickname,
--     r.addr_city,
--     r.fname,
--     r.lname,
--     r.addr_street_1,
--     r.addr_postcode,
--     r.addr_state_id,
--     x.state,
--     r.dob,
--     r.email,
--     NVL((select d.desc from tCustCode d where r.code = d.cust_code),'(None)')
--         as cust_group,
--     e.ext_cust_id,
--     e.master,
--     e.code,
--     rs.shop_no,
--     rs.shop_name,
--     rd.district_no,
--     a.owner,
--     a.owner_type,
--     tw.total_winnings
-- from
--     tAcct a,
--     tCustomerReg r,
--     tCustomer c,
--     outer tCountryState x,
--     outer tExtCust e,
--     outer (tretailshop rs, tretaildistrict rd),
--     outer (select acct_id, sum(winnings) as total_winnings from tbet where winnings > 0 group by acct_id) tw
-- where
--     c.cust_id       = a.cust_id  and
--     r.addr_state_id = x.id       and
--     c.cust_id       = r.cust_id  and
--     c.cust_id       = e.cust_id  and
--     r.shop_id       = rs.shop_id and
--     rs.district_id  = rd.district_id and
--     a.owner         <> 'D'
--     and a.acct_id = tw.acct_id
-- order by
--     c.cust_id
-- ;

-- select first 1
--     *
-- from
--     tbet b,
--     tAcct a
-- where
--     b.winnings > 10
-- ;

/*
select first 10
    a.acct_id,
    tw.total_winnings
from
    tAcct a,
    (select acct_id, sum(winnings) as total_winnings from tbet group by acct_id ) tw
where
    a.acct_id = tw.acct_id
;
*/

/*
select  first 1 
			c.cust_id,
			c.username,
			c.status,
			c.sort,
			c.acct_no,
			c.bet_count - (select count(*) from tbet where acct_id=a.acct_id and status='X') as bet_count,
			c.country_code,
			c.elite,
			a.ccy_code,
			c.cr_date,
			c.max_stake_scale,
			a.balance + a.sum_ap AS balance,
			a.credit_limit,
			r.nickname,
			r.addr_city,
			r.fname,
			r.lname,
			r.addr_street_1,
			r.addr_postcode,
			r.addr_state_id,
			x.state,
			r.dob,
			r.email,
			NVL((select d.desc from tCustCode d where r.code = d.cust_code),'(None)')
				as cust_group,
			e.ext_cust_id,
			e.master,
			e.code,
			rs.shop_no,
			rs.shop_name,
			rd.district_no,
			a.owner,
			a.owner_type,
   --         sum(b.winnings) as total_winnings
		from
			tAcct a,
			tCustomerReg r,
			tCustomer c,
			outer tCountryState x,
			outer tExtCust e,
			outer (tretailshop rs, tretaildistrict rd),
    --        outer tbet b
			
		where
			c.cust_id       = a.cust_id  and
			r.addr_state_id = x.id       and
			c.cust_id       = r.cust_id  and
			c.cust_id       = e.cust_id  and
			r.shop_id       = rs.shop_id and
			rs.district_id  = rd.district_id and
         --   a.acct_id       = b.acct_id and
			a.owner         <> 'D'
			and c.bet_count between 1 and 100
     --   group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
		order by
			c.cust_id
;

		select  first 1000 
			c.cust_id,
			c.username,
			c.status,
			c.sort,
			c.acct_no,
			c.bet_count - (select count(*) from tbet where acct_id=a.acct_id and status='X') as bet_count,
			c.country_code,
			c.elite,
			a.ccy_code,
			c.cr_date,
			c.max_stake_scale,
			a.balance + a.sum_ap AS balance,
			a.credit_limit,
			r.nickname,
			r.addr_city,
			r.fname,
			r.lname,
			r.addr_street_1,
			r.addr_postcode,
			r.addr_state_id,
			x.state,
			r.dob,
			r.email,
			NVL((select d.desc from tCustCode d where r.code = d.cust_code),'(None)')
				as cust_group,
			e.ext_cust_id,
			e.master,
			e.code,
			rs.shop_no,
			rs.shop_name,
			rd.district_no,
			a.owner,
			a.owner_type
			, sum(b.winnings) as total_winnings
		from
			tAcct a,
			tCustomerReg r,
			tCustomer c,
			outer tCountryState x,
			outer tExtCust e,
			outer (tretailshop rs, tretaildistrict rd)
			,outer tbet b
		where
			c.cust_id       = a.cust_id  and
			r.addr_state_id = x.id       and
			c.cust_id       = r.cust_id  and
			c.cust_id       = e.cust_id  and
			r.shop_id       = rs.shop_id and
			rs.district_id  = rd.district_id and
			a.owner         <> 'D'
			and a.acct_id = b.acct_id
            and b.winnings between 1 and 100
	    group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,  14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,  28, 29, 30, 31, 32
		order by
			c.cust_id
;
*/
		select  first 1000 
			c.cust_id,
			c.username,
			c.status,
			c.sort,
			c.acct_no,
			c.bet_count - (select count(*) from tbet where acct_id=a.acct_id and status='X') as bet_count,
			c.country_code,
			c.elite,
			a.ccy_code,
			c.cr_date,
			c.max_stake_scale,
			a.balance + a.sum_ap AS balance,
			a.credit_limit,
			r.nickname,
			r.addr_city,
			r.fname,
			r.lname,
			r.addr_street_1,
			r.addr_postcode,
			r.addr_state_id,
			x.state,
			r.dob,
			r.email,
			NVL((select d.desc from tCustCode d where r.code = d.cust_code),'(None)')
				as cust_group,
			e.ext_cust_id,
			e.master,
			e.code,
			rs.shop_no,
			rs.shop_name,
			rd.district_no,
			a.owner,
			a.owner_type
			, sum(b.winnings) as total_winnings
		from
			tAcct a,
			tCustomerReg r,
			tCustomer c,
			outer tCountryState x,
			outer tExtCust e,
			outer (tretailshop rs, tretaildistrict rd)
			,outer tbet b
		where
			c.cust_id       = a.cust_id  and
			r.addr_state_id = x.id       and
			c.cust_id       = r.cust_id  and
			c.cust_id       = e.cust_id  and
			r.shop_id       = rs.shop_id and
			rs.district_id  = rd.district_id and
			a.owner         <> 'D'
			and a.acct_id = b.acct_id and b.winnings between 1 and 100
			 group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,  14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,  28, 29, 30, 31, 32
		order by
			c.cust_id
