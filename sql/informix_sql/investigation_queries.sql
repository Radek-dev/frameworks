


SELECT TRIM(t.tabname) || '.' || TRIM(c.colname) AS table_dot_column, t.tabname, c.colname
  FROM "informix".systables  AS t
  JOIN "informix".syscolumns AS c ON t.tabid = c.tabid
 WHERE t.tabtype = 'T'
 ORDER BY t.tabname, c.colno;
