package com.example.java;

import java.util.Scanner;

public class SwitchStrings {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        String input = sc.nextLine();
        int monthNumber = Integer.parseInt(input);

        switch (monthNumber) {
            case 1:
                System.out.println("the month is jan");
                break; // must break the switch
            case 2:
                System.out.println("the month is feb");
                break;
            case 3:
                System.out.println("the month is march");
                break;
            default:
                System.out.println("can't handle this");
        }

    }

}
