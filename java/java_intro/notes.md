# Java

## Java - commands

Compile file

```bash
    javac Main.java
```

Give you `class` file, this is the compiled file.

to run the file:

```bash
    java Main
    java Main MyArg # if you with to pass Argument, see Ch03/03_02
```

to change the default java version in linux

```bash
    sudo update-alternatives --config java
```

based on: http://ask.xmodulo.com/change-default-java-version-linux.html

See this for deleting java: https://askubuntu.com/questions/84483/how-to-completely-uninstall-java

## Maven - version control

To control maven at a specific version use:
https://linuxize.com/post/how-to-install-apache-maven-on-ubuntu-18-04/

## Running Jar Files

```bash
    java -jar JarFiles.jar
```
