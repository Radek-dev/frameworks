package com.example.java.model;

// can use these as a variable type
public enum OliveName {
    KALAMATA("Kalamate"), LIGURIAN("Ligurian");

    // this makes sure that we are returning the values from the brackets
    private String name;

    OliveName(String name){
        this.name = name;
    }

    // override the toString as every class have this method
    @Override
    public String toString() {
        return name;
    }
}
