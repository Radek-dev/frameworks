package com.example.java.model;

// model class define the data model, goat create a class that represent single olive
// instance methods and variables to manage the data
public class Olive {
    private String name = "Kalamata";
    private long color = 0x2E0854;
    private int oli = 3;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getColor() {
        return color;
    }

    public void setColor(long color) {
        this.color = color;
    }

    public int crush() {
        System.out.println("Ouch!");
        return oli;
    }

    public void setOli(int oli) {
        this.oli = oli;
    }
}
