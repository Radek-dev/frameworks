Run queries on the DB:


```bash
(django_env) rad@base:~/projects/frameworks/django_project (master)$ python3.7 manage.py makemigrations
Migrations for 'blog':
  blog/migrations/0001_initial.py
    - Create model Post
(django_env) rad@base:~/projects/frameworks/django_project (master)$ python3.7 manage.py sqlmigrate blog 0001
BEGIN;
--
-- Create model Post
--
CREATE TABLE "blog_post" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "title" varchar(100) NOT NULL, "contect" text NOT NULL, "date_posted" datetime NOT NULL, "author_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE INDEX "blog_post_author_id_dd7a8485" ON "blog_post" ("author_id");
COMMIT;
(django_env) rad@base:~/projects/frameworks/django_project (master)$ python3.7 manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, blog, contenttypes, sessions
Running migrations:
  Applying blog.0001_initial... OK
(django_env) rad@base:~/projects/frameworks/django_project (master)$ python3.7 manage.py shell
Python 3.7.8 (default, Jun 29 2020, 05:44:46) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from blog.models import Post
>>> from django.contrib.auth.models import User
>>> User.objects.all()
<QuerySet [<User: rad>, <User: user01>]>
>>> User.objects.first()
<User: rad>
>>> User.objects.filter(username='rad')
<QuerySet [<User: rad>]>
>>> User.objects.filter(username='rad').first()
<User: rad>
>>> udrt = User.objects.filter(username='rad').first()
>>> udrt
<User: rad>
>>> user = User.objects.filter(username='rad').first()
>>> user
<User: rad>
>>> user.id
1
>>> user.ok
Traceback (most recent call last):
  File "<console>", line 1, in <module>
AttributeError: 'User' object has no attribute 'ok'
>>> user.pk
1
>>> user = User.objects.get(id=1)
>>> user
<User: rad>
>>> user
<User: rad>
>>> type(user)
<class 'django.contrib.auth.models.User'>
>>> Post.objects.all()
<QuerySet []>
>>> post_1 = Post(title = 'Blog 1', content = 'First Post Content!', author=user)
Traceback (most recent call last):
  File "<console>", line 1, in <module>
  File "/home/radek.chramosil/projects/venvs/django_env/lib/python3.7/site-packages/django/db/models/base.py", line 500, in __init__
    raise TypeError("%s() got an unexpected keyword argument '%s'" % (cls.__name__, kwarg))
TypeError: Post() got an unexpected keyword argument 'content'
>>> post_1 = Post(title = 'Blog 1', contect = 'First Post Content!', author=user)
>>> Post.objects.all()
<QuerySet []>
>>> post_1.save()
>>> Post.objects.all()
<QuerySet [<Post: Post object (1)>]>

```

More examples

```bash
Python 3.7.8 (default, Jun 29 2020, 05:44:46) [GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from blog.models import Post
>>> from django.contrib.auth.models import User
>>> Post.object.all()
Traceback (most recent call last):
  File "<console>", line 1, in <module>
AttributeError: type object 'Post' has no attribute 'object'
>>> Post.objects.all()
<QuerySet [<Post: Blog 1>]>
>>> user = User.objects.filter(id=1).first()
>>> user
<User: rad>
>>> post_2 = Post(title='Blog 2', content = 'Second Post Constent!', author_id = user.id)
>>> post_2.save()
>>> Post.objects.all()
<QuerySet [<Post: Blog 1>, <Post: Blog 2>]>
>>> post = Post.objects.first()
>>> post.content
'First Post Content!'
>>> post.date_posted
datetime.datetime(2020, 8, 6, 14, 32, 2, 990131, tzinfo=<UTC>)
>>> post.author
<User: rad>
>>> post.author.email
'radek@keemail.me'
>>> .modelname_set
  File "<console>", line 1
    .modelname_set
    ^
SyntaxError: invalid syntax
>>> user
<User: rad>
>>> user.post_set
<django.db.models.fields.related_descriptors.create_reverse_many_to_one_manager.<locals>.RelatedManager object at 0x7f84d26b0410>
>>> user.post_set.all()
<QuerySet [<Post: Blog 1>, <Post: Blog 2>]>
>>> user.post_set.create(title='Blog 3', content = 'Third Post Content!')
<Post: Blog 3>
>>> Post.objects.all()
<QuerySet [<Post: Blog 1>, <Post: Blog 2>, <Post: Blog 3>]>
>>> 
```