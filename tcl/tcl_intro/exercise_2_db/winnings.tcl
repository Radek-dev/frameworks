#!/usr/bin/tclsh

if { $argc > 1 } {
    puts "This script requires one arguement that is a date or no arguement"
    return 1
} else {
    load libOT_InfTcl.so
    set DB [inf_open_conn training@db04_1170]

    if { $argc == 0 } {
        puts "you didn't pass in any arguements"
        set ddate [clock format [clock seconds] -format {%Y-%m-%d}]

        set select_sql {
        INSERT INTO trchramoswins
            SELECT
                    acct_id,
                    extend(cr_date, year to day) as d_date,
                    sum(winnings) as d_wins
            FROM
                    tbet
            WHERE
                    winnings > 0
                    AND
                    extend(cr_date, year to day) != ?
            GROUP BY
                    1, 2;
        }

        # Prepare the statement (in the DB server memory)
        set statement [inf_prep_sql $DB $select_sql]
        # Execute the statement and pass any arguments needed
        set result_set [inf_exec_stmt $statement $ddate]
        # Close the statement
        # This is important to avoid memory leaks on the DB server
        inf_close_stmt $statement

    } elseif { $argc == 1 } {
        set ddate [lindex $argc 0]
        puts "The values is passed in is $ddate"

        set select_sql {
            DELETE FROM trchramoswins
            WHERE extend(cr_date, year to day) = ?;
        }
        set statement [inf_prep_sql $DB $select_sql]
        set result_set [inf_exec_stmt $statement $ddate]
        inf_close_stmt $statement

        set select_sql {
            INSERT INTO trchramoswins
                SELECT
                        acct_id, extend(cr_date, year to day) as d_date, sum(winnings) as d_wins
                FROM
                        tbet
                WHERE
                        winnings > 0
                        AND
                        extend(cr_date, year to day) != ?
                GROUP BY
                        1, 2;
        }
        set statement [inf_prep_sql $DB $select_sql]
        set result_set [inf_exec_stmt $statement $ddate] 
        inf_close_stmt $statement      
    }    
    # Close the result set
    # This is important to avoid memory leaks in the application's memory
    db_close $result_set
    # Close the database connection
    inf_close_conn $DB
}
