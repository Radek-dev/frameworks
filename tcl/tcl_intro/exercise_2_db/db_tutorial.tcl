#!/usr/bin/tclsh

# scp ./winnings.tcl rchramos@dev02:/home/rchramos/exercise_db
# chmod +x winnings.tcl
# ./winnings.tcl

# Load the libraries
load libOT_InfTcl.so
# Open a DB connection
set DB [inf_open_conn training@db04_1170]
# Create a string representing a sql statement
set select_sql {
    select
        c.username,
        r.fname,
        r.lname
    from
        tcustomer c,
        tcustomerreg r
    where
        c.cust_id = ? and
        c.cust_id = r.cust_id
}
set cust_id 12345
# Prepare the statement (in the DB server memory)
set statement [inf_prep_sql $DB $select_sql]
# Execute the statement and pass any arguments needed
set result_set [inf_exec_stmt $statement $cust_id]
# Close the statement
# This is important to avoid memory leaks on the DB server
inf_close_stmt $statement
# Iterate over the result set to get values
for {set i 0} {$i < [db_get_nrows $result_set]} {incr i} {
puts "username is: [db_get_col $result_set $i username]"
puts "first name is: [db_get_col $result_set $i fname]"
puts "last name is: [db_get_col $result_set $i lname]"
}

# Close the result set
# This is important to avoid memory leaks in the application's memory
db_close $result_set
# Close the database connection
inf_close_conn $DB