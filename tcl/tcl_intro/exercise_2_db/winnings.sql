DROP TABLE trchramoswins;

CREATE TABLE trchramoswins (
        acct_id INTEGER,
        cr_date DATE,
        winnings DECIMAL(12,2)
);

ALTER TABLE trchramoswins ADD constraint (
        primary key(acct_id, cr_date)
        constraint crchramoswins_pk
);

ALTER TABLE trchramoswins ADD constraint (
    check (winnings > 0.0)
    constraint crchramoswins_c1
);
