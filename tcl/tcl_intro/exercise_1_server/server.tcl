#!/usr/bin/tclsh

set PING_TIME 10000
set TOTAL_REQS 0

proc basic_server {port} {
    #setup server listening socket
    set sock [socket -server connect_handler $port]
}

#Handles incoming client connections
proc connect_handler {sock addr port} {
    #keep list of connected clients
    global SOCKETS PING_TIME
    set SOCKETS($sock) $sock

    fconfigure $sock -buffering line
    fileevent $sock readable  [list request_handler $sock]

    #start sending pings to client after initial wait
    after $PING_TIME [list send_ping $sock]
}

#Handles input from client sockets
proc request_handler {sock} {

    global SOCKETS TOTAL_REQS

    parray SOCKETS

    if {[eof $sock] || [catch {gets $sock line}]} {
        puts "Client disconnected: $sock"
        close $sock
        unset SOCKETS($sock)
    } else {
        puts "Received: $line"
        incr TOTAL_REQS
        set req [lindex $line 0]
        switch $req {
            quit {
                puts $sock "Closing connection"
                #remove from connected clients
                close $sock
                catch {unset SOCKETS($sock)}
            }
            menu {
                puts $sock [display_menu]
            }
            stime {
                puts $sock [clock format [clock seconds] -format "%H:%M:%S"]
            }
            ltime {
                puts $sock [clock format [clock seconds]]
            }
            num_c {
                puts $sock [array size SOCKETS]
            }
            num_r {
                puts $sock $TOTAL_REQS
            }
            echo {
                puts $sock [lrange $line 1 end]
            }
            default {
                puts $sock "Unrecognised Command"
                puts $sock [display_menu]
            }
        }
    }
}

proc display_menu args {
    return {
        - quit
        - menu
        - stime
        - ltime
        - num_c
        - num_r
        - echo <string>
    }
}

#sends a ping to each connected client every $pingTime ms
proc send_ping {sock} {
    global PING_TIME SOCKETS

    if {[info exists SOCKETS($sock)]} {
        puts $sock {ping}
        after $PING_TIME [list send_ping $sock]
    }
}

#start server
basic_server 1900
vwait forever
puts {If you see this, you're dead}
