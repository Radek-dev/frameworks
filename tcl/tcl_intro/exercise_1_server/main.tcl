#!/usr/bin/tclsh

proc Echo_Server {port} {

	global echo

	set echo(main) [socket -server EchoAccept $port]

}

proc EchoAccept {sock addr port} {

	global echo

	puts "Accept $sock from $addr port $port"

	set echo(addr,$sock) [list $addr $port]

	fconfigure $sock -buffering line

	fileevent $sock readable [list Echo $sock]

}

proc Echo {sock} {

	global echo

	if {[eof $sock] || [catch {gets $sock line}]} {

		# end of file or abnormal connection drop

		close $sock

		puts "Close $echo(addr,$sock)"

		unset echo(addr,$sock)

	} else {

		if {[string compare $line "quit"] == 0} {
			puts $sock "Terminating the client socket"
			
			# Prevent new connections.
			
			# Existing connections stay open.
			
			close $echo(main)

		} elseif {[string compare $line "menu"] == 0} {
			
			puts $sock "
				quit - The client socket is terminated\n
				menu - The menu is displayed\n
				stime - The current server time only is displayed\n
				ltime - The current server date time is displayed\n
				num_c - The number of currently connected clients is displayed\n
				num_r - The total number of requests received from all clients sinc\n
				echo <string> - The string is sent back to the client"
		} elseif {[string compare $line "stime"] == 0} {
			
			set systemTime [clock seconds]

			puts $sock "The server time is: [clock format $systemTime -format %H:%M:%S]"

		} elseif {[string compare $line "ltime"] == 0} {
			
			set systemTime [clock seconds]

			puts $sock "The server date is: [clock format $systemTime -format %D]"
			puts $sock [clock format $systemTime -format {Server time: %A, the %d of %B, %Y}]

		} elseif {[string compare $line "num_c"] == 0} {
			puts $sock "Number of connections is: [array size echo]"
		
		} elseif {[string compare $line "num_r"] == 0} {
			puts "hi"
		
		} elseif {[string compare $line "echo"] == 0} {
			puts "hi"
		
		} else {

			puts $sock $line

		}
	}

}
puts "Started the server"
Echo_Server 2540
vwait forever
