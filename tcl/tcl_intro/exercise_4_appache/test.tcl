set PEOPLE(0,name) "John"
set PEOPLE(1,name) "Tom"
set PEOPLE(2,name) "Will"
set PEOPLE(0,age) 23
set PEOPLE(1,age) 34
set PEOPLE(2,age) 44

parray PEOPLE

puts $PEOPLE(0)

set a 0,name


proc construct_sql {level} {
    set QUERIES(top) "
        select
            ctg.name as output
        from 
            tevcategory ctg"
    
    set QUERIES(category) "select
            cl.name as output
        from
            tevcategory ctg,
            tevclass cl
        where
            ctg.category = cl.category
            and ctg.name = ?"
    # parray QUERIES
    return $QUERIES($level)
}

parray QUERIES($level)

puts $QUERIES($level)


set level top
puts $level
construct_sql level


set sql [construct_sql level]

puts $sql

set  hierarchy  [dict create \
    top      "category"  \
    category "class"     \
    class    "type"      \
    type     "event"     \
    event    "market"    \
    market   "selection" \
    ]
set level [dict get $hierarchy class]
puts $level

set colours [dict create colour1 "black" colour2 "white"]
set value [dict get $colours colour1]
puts $value


set aa ""
puts $aa

set aaa {}
puts $aaa
