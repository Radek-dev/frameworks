# Appache + Other server set up

## Notes

### To start the server

```bash
rchramos@dev02:~$ cd git_src/induction/training/admin/
rchramos@dev02:~/git_src/induction/training/admin$ appserv admin_local_rchramos.cfg
```

Web
<https://dev02.openbet/rchramos.admin>

Login:
Administrator
1ncharge

### To restart the server

1. check if the config files for apache server are fine
`apachectl -t`
-> sysntax OK

2. restart restart the server to load in the config files
`sudo apachectl graceful`

the file are in:
`rchramos@dev02:~/git_src/induction/training/admin/`

### To kill the server

to find the process id:

Examples:

```bash
ps
ps -i # gives you all
```

to get the ID:

```bash
ps -ef | grep admin_local_rchramos.cfg
->
rchramos  3392 20370  0 09:43 pts/200  00:00:00 grep --color=auto admin_local_rchramos.cfg
rchramos 27693 24968  0 09:27 pts/212  00:00:00 ot_appserv admin_local_rchramos.cfg
rchramos 27714 27693  0 09:27 pts/212  00:00:00 ot_appserv -child 0 admin_local_rchramos.cfg
rchramos 27754 27693  0 09:27 pts/212  00:00:00 ot_appserv -child 1 admin_local_rchramos.cfg
rchramos 27769 27693  0 09:27 pts/212  00:00:00 ot_appserv -child 2 admin_local_rchramos.cfg
```

Then bash

```bash
kill 3392
kill <the process ID>
```

## Technical Notes

`init.tcl` - runs the first
creates a pass to system folder directories that are needed for the website

loads packages

does some checks

req_init - request init
    runs first when you make a new request

req_end - clean up after the request

training.tcl
action definitions
    the app server will be able to pick these actions ups

procedure code
    what the code does

## Optimise queries

```bash
ssh rchramos@dev02
dbaccess training -
set explain on
run query
```

Then you have file `explain.out` in the dev server.

```bash
ssh rchramos@db04
cd ~
vim explain.out
```

## Use `sftp` to access the dev linux

If u go to Files -> Other Locations

On the bottom u have Connect to server

u insert: `sftp://dev02/`
