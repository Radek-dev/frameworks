#!/usr/bin/tclsh
load libOT_InfTcl.so

global CACHE DB CACHE_TIMEOUT

set CACHE_TIMEOUT 15

set DB [inf_open_conn willhill_dev@db01_1150]

proc event_server {port} {
    #setup server listening socket
    set sock [socket -server connect_handler $port]
}

#Handles incoming client connections
proc connect_handler {sock addr port} {
    #keep list of connected clients
    global sockets
    set sockets($sock) $sock

    fconfigure $sock -buffering line
    fileevent $sock readable  [list request_handler $sock]
}

#Handles input from client sockets
proc request_handler {sock} {

    global sockets DB CACHE_TIMEOUT CACHE

    if {[eof $sock] || [catch {gets $sock line}]} {
        puts "Client disconnected: $sock"
        close $sock
        unset sockets($sock)
    } else {
        puts "Received: $line"
        if {![sanity_check $line]} {
            puts $sock "Unrecognised Command"
            return
            }
    
        if {[info exists CACHE($line)]} {
            puts {output from cache}
            puts $sock $CACHE($line)
            return
        }
    
        set sport     [lindex $line 0]
        set settled   [lindex $line 1]
        set date_from [lindex $line 2]
        set date_to   [lindex $line 3]
        
        set settled [string map {S Y U N} $settled]
    
        set sql {
            select
                e.ev_id,
                e.desc,
                e.start_time
            from
                tev e,
                tevclass c
            where
                e.ev_class_id = c.ev_class_id and
                c.sort = ? and
                extend(e.cr_date, year to day) between ? and ? and
                e.settled = ?
        }
    
        set statement  [inf_prep_sql $DB $sql]
        set result_set [inf_exec_stmt $statement $sport $date_from $date_to $settled]
        inf_close_stmt $statement
    
        set main_list [list]
        for {set i 0} {$i < [db_get_nrows $result_set]} {incr i} {
            set ev_id      [db_get_col $result_set $i ev_id]
            set desc       [db_get_col $result_set $i desc]
            set start_time [db_get_col $result_set $i start_time]

            lappend main_list [list $ev_id $desc $start_time]
        }

        db_close $result_set
    
        set CACHE($line) $main_list
        
        after [expr $CACHE_TIMEOUT * 1000] [list unset CACHE($line)]
        
        puts {output from DB}
        
        puts $sock $main_list
    }
}

proc sanity_check {request} {
    if {[llength $request] != 4} {
        return 0
    }

    if {[regexp {FB|HR|GR S|U [0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{4}-[0-9]{2}-[0-9]{2}} $request]} {
        return 1
    }
    return 0
}

#start server
event_server 1550

vwait forever
inf_close_conn $DB

puts {If you see this, you're dead}