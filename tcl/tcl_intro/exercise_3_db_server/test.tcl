#!/usr/bin/tclsh

# Using the database - this is working 

# Load the libraries
load libOT_InfTcl.so
# Open a DB connection
set DB [inf_open_conn training@db04_1170]
# Create a string representing a sql statement
set select_sql {
select first 10
    e.ev_id,
    e.start_time,
    e.desc
from tev e;
where e.ev_id = ?
}
set cust_id {10 6}
# Prepare the statement (in the DB server memory)
set statement [inf_prep_sql $DB $select_sql]
# Execute the statement and pass any arguments needed
set result_set [inf_exec_stmt $statement [split $cust_id]]
# Close the statement
# This is important to avoid memory leaks on the DB server
inf_close_stmt $statement
# Iterate over the result set to get values

puts $result_set

for {set i 0} {$i < [db_get_nrows $result_set]} {incr i} {
puts "username is: [db_get_col $result_set $i ev_id]"
puts "first name is: [db_get_col $result_set $i start_time]"
puts "last name is: [db_get_col $result_set $i desc]"
}

set lst {}
for {set i 0} {$i < [db_get_nrows $result_set]} {incr i} {
    lappend lst [db_get_col $result_set $i ev_id] [db_get_col $result_set $i start_time] [db_get_col $result_set $i desc]
}

puts "\n $lst"

# Close the result set
# This is important to avoid memory leaks in the application's memory
db_close $result_set
# Close the database connection
inf_close_conn $DB


# list
set a {ah Ebh az}

split $a " "

set lst {}

foreach j $a {
    lappend lst j
}

puts $lst

set settled S
if {$settled == "U"} {
        set settled [regsub "U" $settled N]
} else {
    set settled [regsub "S" $settled Y]
}

global CACH

set CACH(a) 111
set CACH(b) 222
set lst {1 2 3}
set line {a b}

parray CACH

if {[info exists CACH(a)]} {
    puts 1
}

proc create_cach {line lst} {
    global CACH
    set CACH($line) $lst
    after 10000 [list remove_line $line]
}

proc remove_line {line} {
    global CACH
    unset CACH($line)
    puts "this run"
}

create_cach $line $lst

parray CACH

unset CACH

FB U 2012-09-01 2012-09-28