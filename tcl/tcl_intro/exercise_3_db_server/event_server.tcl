#!/usr/bin/tclsh

load libOT_InfTcl.so
global CACHE

proc basic_server {port} {
    #setup server listening socket
    set sock [socket -server connect_handler $port]
}

#Handles incoming client connections
proc connect_handler {sock addr port} {
    #keep list of connected clients
    global SOCKETS
    set SOCKETS($sock) $sock

    fconfigure $sock -buffering line
    fileevent $sock readable  [list request_handler $sock]
}

proc add_to_CACHE {line lst} {
    puts "line added to CACHE: $line"
    global CACHE
    set CACHE($line) $lst
    after 30000 [list remove_line $line]
}

proc remove_line {line} {
    puts "line removed from CACHE: $line"
    global CACHE
    unset CACHE($line)
}

#Handles input from client sockets
proc request_handler {sock} {

    global SOCKETS CACHE

    parray SOCKETS

    if {[eof $sock] || [catch {gets $sock line}]} {
        puts "Client disconnected: $sock"
        close $sock
        unset SOCKETS($sock)
    } else {
        puts "Received: $line"
        # FB U 2012-09-01 2012-09-28
        # FB S 2012-09-01 2012-09-28

        if {[info exists CACHE($line)]} {
            puts "Cached data accessed for: $line"
            puts $sock $CACH($line)
        } else {
            set category [lindex $line 0]
            set settled [lindex $line 1]
            if {$settled == "U"} {
                set settled [regsub "U" $settled N]
            } else {
                set settled [regsub "S" $settled Y]
            }
            set date_from [lindex $line 2]
            set date_to [lindex $line 3]

            set DB [inf_open_conn training@db04_1170]
            set sql {
                select
                    e.ev_id,
                    e.settled,
                    e.start_time as date
                from
                    tev e,
                    tevtype t,
                    tevclass c
                where 
                    c.sort = ?
                    and e.settled = ?
                    and extend(e.cr_date, year to day)
                        between ? and ?
                    and e.ev_type_id = t.ev_type_id
                    and c.ev_class_id = t.ev_class_id
            }
            set statement [inf_prep_sql $DB $sql]
            set result_set [inf_exec_stmt $statement $category $settled $date_from $date_to]

            set lst {}
            for {set i 0} {$i < [db_get_nrows $result_set]} {incr i} {
                lappend lst [db_get_col $result_set $i ev_id] [db_get_col $result_set $i settled] [db_get_col $result_set $i date]
            }
            add_to_cach $line $lst
            puts $sock $lst

            inf_close_stmt $statement
            db_close $result_set
            inf_close_conn $DB
        }
    }
}

#start server
basic_server 1900
vwait forever
