/*
select
    c.sort,
    e.settled,
    extend(e.cr_date, year to day) as date
from
    tev e,
    tevtype t,
    tevclass c
where 
    c.sort = "FB"
    and e.settled = "N"
    and extend(e.cr_date, year to day)
        between "2012-09-01" and "2012-09-28"
    and e.ev_type_id = t.ev_type_id
    and c.ev_class_id = t.ev_class_id
;


select first 10
    e.ev_id,
    e.start_time,
    e.desc
from tev e;

select
    c.cust_id,
    c.username,
    c.password,
    r.fname,
    r.lname,
    c.bet_count
from
    tcustomer c,
    tcustomerreg r
where
    c.cust_id = r.cust_id
    and c.cust_id = 888;

*/

select
    b.receipt
from
    tcustomer c,
    tacct a,
    tbet b
where
    c.cust_id = a.cust_id
    and a.acct_id = b.acct_id
    and c.cust_id = 100;