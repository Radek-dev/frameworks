#!/usr/bin/tclsh
#
# Unit test harness file for the payment package
#
load libOT_Tcl.so
# Figure out the root directory of this module for use later...
set basedir [file join [file dirname [info script]] "../../.."]
puts "based dir is: $basedir"
# Make sure the core module and the local code is in the auto_path...
lappend auto_path "$basedir/core"
lappend auto_path "$basedir/src/main/tcl"
# Load the unit testing framework...
package require core::unit
# Initial logging for a standalone script...
core::log::init -log_dir "$basedir/target/test-logs" -log_file "payment-tests.log" –standalone 1
core::unit::init
# Load the source code we want to test...
package require payment
# Create a test suite to group other tests...
core::unit::testsuite -name payment-tests
# Test case...
core::unit::test -name {Test for payment::is_valid_amount(0.00)} -classname payment::is_valid_amount}
-body {
if {[payment::is_valid_amount 0.00] != 0} {
error "Returned wrong result"
}
}
# Test case...
core::unit::test -name {Test for payment::is_valid_amount(1.00)} -classname payment::is_valid_amount}
-body {
if {[payment::is_valid_amount 1.00] != 1} {
error "Returned wrong result"
}
}
# Test case...
core::unit::test -name {Test for payment::is_valid_amount(-1.00)} -classname
{payment::is_valid_amount} -body {
if {[payment::is_valid_amount -1.00] != 0} {
error "Returned wrong result"
}
}
# Write out the JUnit XML
core::unit::write -output "$basedir/target/test-reports/payment-tests.xml"