set t1 [dict create a b c d]
puts "Start Dict is: $t1"

set t2 [dict replace $t1 a A]
puts "After Replace: $t2"

set t3 [dict replace $t1 a A b B c C]
puts "After Replace with unmatched key b: $t3"

dict update t1 a aa c cc {
  if {[info exists aa]} {set aa B}
  if {[info exists cc]} {set cc D}
}

puts "t1 after update: $t1"

set t1 {a b c d e f}
dict with t1  {
  puts "A: $a C: $c E: $e"
  puts "Does value exist? [info exists b] (0 = No)"
  set a AA
  set c CC
  set e EE
}

set t1 [dict create a {b c} d {e {f g}}]

# Look at first element:
puts "Contents of dict value for key=a: [dict with t1 a {puts $b}]"

puts "Contents of dict value for key=d: [dict with t1 d {puts $e}]"

puts "Contents of dict value for key=d/e: [dict with t1 d e {puts $f}]"

