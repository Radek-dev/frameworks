# https://tcl.tk/man/tcl8.5/tutorial/Tcl6.html

set X 100;
set Y 256;
set Z [expr "$Y + $X"]
set Z_LABEL "$Y plus $X is "

puts "$Z_LABEL $Z"
puts "The square root of $Y is [expr sqrt($Y)]\n"

puts "Because of the precedence rules \"5 + -3 * 4\"   is: [expr -3 * 4 + 5]"
puts "Because of the parentheses      \"(5 + -3) * 4\" is: [expr (5 + -3) * 4]"

puts "\n................. more examples of differences between  \" and \{"
puts {$Z_LABEL [expr $Y + $X]}
puts "$Z_LABEL {[expr $Y + $X]}"
puts "The command to add two numbers is: \[expr \$a + \$b]"


set X 100
set Y 256
set Z [expr {$Y + $X}]
set Z_LABEL "$Y plus $X is "

puts "$Z_LABEL $Z"
puts "The square root of $Y is [expr { sqrt($Y) }]\n"

puts "Because of the precedence rules \"5 + -3 * 4\"   is: [expr {-3 * 4 + 5}]"
puts "Because of the parentheses      \"(5 + -3) * 4\" is: [expr {(5 + -3) * 4}]"

set A 3
set B 4
puts "The hypotenuse of a triangle: [expr {hypot($A,$B)}]"

#
# The trigonometric functions work with radians ...
#
set pi6 [expr {3.1415926/6.0}]
puts "The sine and cosine of pi/6: [expr {sin($pi6)}] [expr {cos($pi6)}]"

#
# Working with arrays
#
set a(1) 10
set a(2) 7
set b    2
set a(3) 17
puts "Sum: [expr {$a(1)+$a($b)}]"