
puts " dict set test a 1 b 2 c 3 returns:"
puts [dict set test a 1 b 2 c 3]
puts "The value associated with 'a' is: [dict get $test a]"
puts "The value associated with 'a 1' is: [dict get $test a 1]\n"

dict set books 1 first Clif
dict set books 1 last Flynt
dict set books 1 title "Tcl/Tk For Real Programmers"

dict set books 2 [list first Clif \
    last Flynt \
    title "Tcl/Tk: A Developers Guide"]

foreach {id first last title} {
    3      Brent    Welch     {Practical Programming in Tcl/Tk}
    4      Michael  McClennan {Effective Tcl/Tk Programming}
    5      Don      Libes     {Exploring Expect}  } {
  dict set books $id [list first $first last $last title $title]
}

set first firstTest
dict for {id info} $books {
  dict with info {
    puts "$id: $last,$first  \"$title\""
  }
}
puts "after dict for: first is $first"