proc example {first {second ""} args} {
  if {$second == ""} {
    puts "There is only one argument and it is: $first";
    return 1;
    } else {
    if {$args == ""} {
      puts "There are two arguments - $first and $second";
      return 2;
      } else {
      puts "There are many arguments - $first and $second and $args";
      return "many";
      }
    }