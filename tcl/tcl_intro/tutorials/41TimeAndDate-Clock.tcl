# https://tcl.tk/man/tcl8.5/tutorial/Tcl41.html

;# Get the time in system seconds
set systemTime [clock seconds]

;# Display that time using the hour, minute and second descriptors.
puts "The time is: [clock format $systemTime -format %H:%M:%S]"

;# Display the date using a shorthand format
puts "The date is: [clock format $systemTime -format %D]"

;# You can use complex format statements.
puts [clock format $systemTime -format {Today is: %A, the %d of %B, %Y}]

;# And, this is the default output for the time, with no -format
puts "\n the default format for the time is: [clock format $systemTime]\n"

;# The book and movie versions of 2001 had different dates for when the
;# HAL 9000 became alive.
;# We can use the scan command to figure out how many seconds they differed by

set halBirthBook "Jan 12, 1997"
set halBirthMovie "Jan 12, 1992"
set bookSeconds [clock scan $halBirthBook]
set movieSeconds [clock scan $halBirthMovie]

puts "The book and movie versions of '2001, A Space Oddysey' had a"
puts "discrepency of [expr $bookSeconds - $movieSeconds] seconds in how"
puts "soon we would have sentient computers like the HAL 9000"