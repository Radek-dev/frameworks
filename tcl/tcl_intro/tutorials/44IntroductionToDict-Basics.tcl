#Create a dict
set dict1 [dict create a 1 b 2]

# Assign a couple of key/value pairs.
dict set dict1 c 3
dict set dict1 d 4

# How many pairs are in this dict
puts "there are [dict size $dict1] pairs in dict1"

# What are the keys and values
puts "The keys are: [dict keys $dict1]"
puts "The values are: [dict values $dict1]"

puts "\nShow the key/value pairs using get"
foreach key [dict keys $dict1] {
  puts "The value associated with $key is [dict get $dict1 $key]"
}
puts "\nShow the key/value pairs using two loop variables."
foreach key [dict keys $dict1]  val [dict values $dict1] {
  puts "The value associated with $key is $val"
}
