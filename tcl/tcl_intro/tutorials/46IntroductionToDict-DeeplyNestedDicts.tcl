# Build nested dict
# Keys are:
#  Unique Value
#    AUTHOR
#    SERIES
#      series name
#        book title
#          owned
#          read

# Build dict element 1 using multiple dict set commands

dict set books 1 AUTHOR "P.G. Wodehouse"
dict set books 1 SERIES "Blandings Castle" "Something Fresh" owned no
dict set books 1 SERIES "Blandings Castle" "Heavy Weather" owned yes
dict set books 1 SERIES "Jeeves & Wooster" "The Inimitable Jeeves" owned no
dict set books 1 SERIES "Jeeves & Wooster" "Carry On, Jeeves" owned yes

# Build dict element 2 using embedded dict create commands.

dict set books 2 \
  [dict create AUTHOR "Edgar Rice Burroughs"\
    SERIES \
    [dict create Barsoom \
      [dict create "Princess Of Mars" [dict create owned no read no] \
                   "Warlord Of Mars" [dict create owned yes read no]] \
          Tarzan \
      [dict create "Tarzan of the Apes" [dict create owned yes read yes] \
                   "The Return of Tarzan" [dict create owned no read yes]] \
          Venus \
      [dict create "Pirates of Venus" [dict create owned yes read yes] \
                   "Lost on Venus" [dict create owned no read yes]] \
    ] \
  ]


# Display the book titles in the Blandings Castle series.
puts "These books are in the Blandings Castle series: \
    [dict keys [dict get $books 1 SERIES {Blandings Castle}]]"

# Add a field to the Blandings Castle series for 'read'

dict set books 1 SERIES {Blandings Castle} {Something Fresh} read no
dict set books 1 SERIES {Blandings Castle} {Heavy Weather} read yes

foreach mainKey [dict keys $books] {
  set author [dict get $books $mainKey AUTHOR]
  dict for {k v} [dict get $books $mainKey SERIES] {
    # k has the name of the book (key) 
    # v is a dict with title as key 
    dict for {k1 v1} $v {
      # k1 is the title of the book.
      # v1 is a dict with 'read' and 'owned' and keys
      puts "Author: $author"
      puts "Series Title: $k"
      puts "  Book Title: $k1" 
	puts -nonewline "  "
      dict for {k2 v2} $v1 {
        # k2 will be 'owned' or 'read'
	# v2 will be 'yes' or 'no'
	puts -nonewline "$k2 $v2 "
      }
      puts ""
    }
  }
}
