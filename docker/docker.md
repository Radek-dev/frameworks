# Docker Intro

```bash
docker help
docker build --help
```

Use this to get more help.

```bash
docker build --help
```

List running containers:

```bash
docker ps
```

Check also the `docker-compose ps`.

```bash
docker-compose ps
```

Forcefully delete all images:

```bash
docker rmi $(docker images -q) --force
```

List running Containers

```bash
docker ps
```

Stopping running Containers (and re-starting them)

```bash
docker stop <container_name or container_id>
docker ps -a
```

Attaching to running container

```bash
docker start <container_name or container_id>
docker attach <container_name or container_id>
```

Removing stopped Containers

```bash
docker rm <container_name or container_id>
```

Deataching from running container

```bash
Ctrl + P + Ctrl + Q
```

Run external commands to running container

```bash
docker exec <container_name> <command>
# like:
docker exec fed1 touch my_file.txt
```

Create new image from container

```bash
docker commit fed1 <imagename_to_be_created>
```

Docker Containers: Volumes
Case 1. Copy an external file from the Host to a running Container
Case 2. Create a shared volume between the Host and the Container
Case 3. Create a shared volume to be used between Containers

```bash
docker cp <full path to file to copy> ub1:/<name of file to becreated in ub1>
docker run -it --name ub1 -v <full path to external folder>:/<folder in container> ubuntu bash
docker run -ti –-name ub1 –v /shared_folder ubuntu bash
docker run -ti –-name ub2 –-volumes-from ub1 ubuntu bash
```

Docker Containers: Ports

```bash
docker run -ti --rm --name nc1 ubuntu:14.04 bash
nc –lp 1234
```

Other docker:

```bash
docker run -ti --rm –-link nc1 --name nc2 ubuntu:14.04 bash
nc nc1 1234
```

Docker Files

```docker
FROM golang:1.9.2-alpine3.6 AS build

# Install tools required to build the project
# We need to run `docker build --no-cache .` to update those dependencies
RUN apk add --no-cache git
RUN go get github.com/golang/dep/cmd/dep

# Gopkg.toml and Gopkg.lock lists project dependencies
# These layers are only re-built when Gopkg files are updated
COPY Gopkg.lock Gopkg.toml /go/src/project/
WORKDIR /go/src/project/
# Install library dependencies
RUN dep ensure -vendor-only

# Copy all project and build it
# This layer is rebuilt when ever a file has changed in the project directory
COPY . /go/src/project/
RUN go build -o /bin/project

# This results in a single layer image
FROM scratch
COPY --from=build /bin/project /bin/project
ENTRYPOINT ["/bin/project"]
CMD ["--help"]
```

## CheatSheet

| First Header  | Second Header |
| ------------- | ------------- |
| docker attach   | Attach to a running container   |
| docker build   | Build an image from a Dockerfile   |
| docker commit   | Create a new image from a container’s changes   |
| docker config   | Manage Docker configs   |
| docker container   | Manage containers   |
| docker cp   | Copy files/folders between a container and the local filesystem   |
| docker exec   | Run a command in a running container   |
| docker export   | Export a container’s filesystem as a tar archive   |
| docker image   | Manage images   |
| docker images   | List images   |
| docker import   | Import the contents from a tarball to create a filesystem image   |
| docker info   | Display system-wide information   |
| docker inspect   | Return low-level information on Docker objects   |
| docker kill   | Kill one or more running containers   |
| docker logs   | Fetch the logs of a container   |
| docker network   | Manage networks   |
| docker pause   | Pause all processes within one or more containers   |
| docker port   | List port mappings or a specific mapping for the container   |
| docker ps   | List containers   |
| docker pull   | Pull an image or a repository from a registry   |
| docker push   | Push an image or a repository to a registry   |
| docker rename   | Rename a container   |
| docker restart   | Restart one or more containers   |
| docker rm   | Remove one or more containers   |
| docker rmi   | Remove one or more images   |
| docker run   | Run a command in a new container   |
| docker save   | Save one or more images to a tar archive (streamed to STDOUT by default)   |
| docker search   | Search the Docker Hub for images   |
| docker start   | Start one or more stopped containers   |
| docker stats   | Display a live stream of container(s) resource usage statistics   |
| docker stop   | Stop one or more running containers   |
| docker top   | Display the running processes of a container   |
| docker unpause   | Unpause all processes within one or more containers   |
| docker update   | Update configuration of one or more containers   |
| docker version   | Show the Docker version information   |
| docker volume   | Manage volumes   |
