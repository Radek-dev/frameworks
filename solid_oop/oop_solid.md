Based on JAVA

Names for classes - use nouns

# Fragile Class - avoid:

- always worry about changes to the super class that may break the subclass - real problem

* Don't use extends - don't use superclass
    - use final - so you can't extend
    - if you need to extend class, declare methods final if it is dangerous to extend the methods

* Only use extends only when subclass is exactly like the super class but does only a little more
    - it should be okay to treat subclass as superclass


# Defining Class Contracts:

an interface is a contract
class contracts embody both structure and behavior
- meaning syntax, comments and return values are like

Interface Segregation Principle - split up your interfaces in such a way that you don't have to implement something that you can't implement

names for interfaces - use adjective

Can implement just the method from the superclass and not the whole class

Interfaces gives us choices what to in subclasses

Implement interface to subclass to avoid the fragility

Replace extends with implements when ever possible


# Dependency Inversion ?

if you change A, you have to change B - one direction dependencies usually

