
delete on remote
```bash
     git push -d origin <branch_name>
```

merge squash
```bash
     git checkout master
     git checkout -b feature/WIL-46290_temp
     git merge --squash feature/WIL-46290
     git commit -am "WIL-46290 New Admin User for GTP (central feeds)"
     git branch -m feature/WIL-46290 feature/WIL-46290_unsquashed
     git branch -m feature/WIL-46290
     git branch -D feature/WIL-46290_unsquashed
     git push --force
     git push --set-upstream --force origin feature/WIL-64632_remove_greater_than_1hour_periods
 ```
or:

```bash
     git rebase -i HEAD~<number of commits>
```
Note: Check on `git reflog` in order to undo the squash

or:
```bash
     git checkout release/INTEGRATION_PP1_20190117
     git pull
     git checkout -b feature/WIL-ΧΧΧΧΧ_PP1_Integration
     git merge --squash origin/feature/WIL-ΧΧΧΧΧ   # (resolve conflicts, should not have any)
     git status
     git add
     git commit -m "WIL-...."
     git checkout - # (this is in this case git checkout release/INTEGRATION_PP1_20190117)
     git merge feature/WIL-50283_PP1_Integration
     git push
```

revert last commit:

```bash
     git reset --hard HEAD~1
```

revert back to specific commit:

```bash
     git reset --hard 0d1d7fc32
```

delete branch locally
```bash
git branch -d localBranchName
```

delete branch remotely
```bash
git push origin --delete remoteBranchName
```

remember this book:
https://git-scm.com/book/en/v2


Fixing the conflicts with master:

Go to the repo root folder and checkout master

```bash
git pull
git checkout my-dev-branch
git rebase origin/master
 -- conflict resolution --
git add .
git rebase --continue
git log --oneline (new commit should be on top)
git push -f
```