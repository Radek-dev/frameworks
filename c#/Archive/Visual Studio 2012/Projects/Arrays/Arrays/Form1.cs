﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            //declare an array
            int[] lotteryNumbers;
            lotteryNumbers = new int[49];
       
            //the above is the same as
            //int[] lotteryNumbers = new int [10];

            //assign values to the array
            //lotteryNumbers[0] = 1;
            //lotteryNumbers[1] = 2;
            //lotteryNumbers[2] = 3;
            //lotteryNumbers[3] = 4;

            //this is an alternative declaration for the whole array with its values
            //int[] lotteryNumbers = { 1, 2, 3, 4 };

            for (int i = 0; i != lotteryNumbers.Length; i++)
            {
                 lotteryNumbers[i] = i + 1;

                 //display the numbers
                 listBox1.Items.Add(lotteryNumbers[i]);


             }


            //remember that it can hold only one array at a time
            //remember that it can assing values just from the 0,0 (first) element
            //multidimensional array
            int[ , ] array2D;
            array2D = new int[5, 3];

            array2D[0, 0] = 100;

            listBox1.Items.Clear();
            listBox1.Items.Add(array2D[0, 0]);

            //string array

            string[] arrayStrings = new string[5];

            arrayStrings[0] = "This";
            arrayStrings[1] = "is";
            arrayStrings[2] = "a";
            arrayStrings[3] = "string";
            arrayStrings[4] = "array";

            //just to clear it
            listBox1.Items.Clear();
            //foreach loop-must have a declaration for the the new variable
            foreach (string sLine in arrayStrings)
            {
                listBox1.Items.Add(sLine);
            }

 

        }
    }
}
