﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loops
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int answer;

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //declare the variables
            int loopStart;
            int loopEnd;
            int multiplyBy;

            //testing for the correct values in the text boxes

            int outputValue = 0;
            bool isNumber = false;

            //
            isNumber = int.TryParse(textBox1.Text, out outputValue);

            if (!isNumber)
            {
                MessageBox.Show("Type numbers in the text boxes");
            }

            isNumber = int.TryParse(textBox2.Text, out outputValue);

            //read this is if isNumber is not true then do the things - the same as 'if (isNumber == false)'
            if (!isNumber)
            {
                MessageBox.Show("Type numbers in the text boxes");
            }

            isNumber = int.TryParse(textBox3.Text, out outputValue);

            if (!isNumber)
            {
                MessageBox.Show("Type numbers in the text boxes");
            }



            //read in the values
            loopStart = int.Parse(textBox1.Text);
            loopEnd = int.Parse(textBox2.Text);
            multiplyBy = int.Parse(textBox3.Text);

            //declare the integer for the second loop
            int iForDoLoop = loopStart;

            //declare the integer for the while loop (third one)
            int iForWhileLoop = loopStart;

            //clear the list box
            listBox1.Items.Clear();

            //for loop - first loop
            for (int i = loopStart; i <= loopEnd; i++)
            {

                answer = multiplyBy * i;

                listBox1.Items.Add(i + " times "  + multiplyBy + " = " + answer.ToString());

            }

            //do loop - second loop
            do
            {

                answer = multiplyBy * iForDoLoop;
                listBox1.Items.Add(answer.ToString());
                iForDoLoop++;

            } while (iForDoLoop <= loopEnd);

            //while loop - third loop
            while (iForWhileLoop <= loopEnd)
            {

                answer = multiplyBy * iForWhileLoop;
                listBox1.Items.Add(answer.ToString());
                iForWhileLoop++;

            }

        }
    }
}
