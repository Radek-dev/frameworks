﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methods
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddUp(int.Parse(textBox1.Text),int.Parse(textBox2.Text));
        }

        //methods is like a function however it does not have return a value as the below one

        void AddUp(int firstNumber, int SecondNumber)
        {

            MessageBox.Show("Add Up Here: " + (firstNumber + SecondNumber).ToString());

            return;
        }

        //private method can't be called from outside this class, the class is 'public partial class Form1 : Form
        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Subtract(int.Parse(textBox1.Text), int.Parse(textBox2.Text)).ToString());
        }

        private int Subtract(int fisrtNumber, int secondNumber)
        {
            return fisrtNumber - secondNumber;
        }

    }
}
