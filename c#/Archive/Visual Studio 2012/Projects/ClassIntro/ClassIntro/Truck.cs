﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassIntro
{
    //inherinting from the Auto Class - change Auto and all the derived classes will inherent that, can overwrite the base classes on the specialisation
    //Auto class is the base, parent class - generalisation
    //Truck is the derived, childe class - specialisation

    class Truck : Auto
    {
        //add specific properties and methods for track
        //auto is generalisation
        //Truck is specialisation of Auto

        public int TowingCapacity;
        public int CargoCapacity;

        public void Tow()
        {
            Console.WriteLine("Now tow...");
        }

        public void Haul()
        {
            Console.WriteLine("Now hauling ...");
        }
        
        //overwrite a method from the generalisation of the class, the parent class is hiden currently

        //virtual means if derived implementation of this method exists, then use it. specialised method
        public virtual void Start()
        {
            //use 'base' to refer to the original method
            base.Start();
            Console.WriteLine("Truck ignition sequence...");
        }


    }
}
