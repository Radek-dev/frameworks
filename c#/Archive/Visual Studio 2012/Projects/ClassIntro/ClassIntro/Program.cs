﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassIntro
{
    class Program
    {
        static void Main(string[] args)
        {
            //declare an instance of the class 'Auto'
            Auto myCar = new Auto();
            myCar.CurrentSpeed = 0;
            myCar.Maker = "Oldsmobile";
            myCar.Model = "Cutlas Supreme";
            myCar.Year = 1988;
            myCar.Color = "Silver";
            myCar.Miles = 80000;

            Console.WriteLine(myCar.Maker + " " + myCar.Model);

            int myCurrentSpeed = 0;

            myCurrentSpeed = myCar.Accelerate(3);

            Console.WriteLine("My current speed: " + myCurrentSpeed.ToString());


            //call the method from the class, pass the values
            myCar.Accelerate(5);
            myCar.Accelerate(6);
            myCar.Accelerate(2);
            myCar.Accelerate(1);

            //call the method in the class, pass the values
            string message = myCar.SpeedLimitVialation("Warning: ", 20);
            Console.WriteLine(message);

            myCurrentSpeed = myCar.Decelerate(200);

            Console.WriteLine("My current speed: " + myCurrentSpeed.ToString());

            Console.ReadLine();




            //create engiens

            Engine v4 = new Engine();
            v4.Size = "v4";
            v4.HorsePower = 200;
            v4.FuelConsumptionRate = 1.6;

            Engine v6 = new Engine();
            v6.Size = "v6";
            v6.HorsePower = 350;
            v6.FuelConsumptionRate = 2.6;

            Engine v8 = new Engine();
            v8.Size = "v8";
            v8.HorsePower = 500;
            v8.FuelConsumptionRate = 3.7;


            Auto myMyCar = new Auto();

            myMyCar.Engine = v6;

            Console.WriteLine(myMyCar.Engine.HorsePower.ToString());
            Console.ReadLine();

            myMyCar.Customer.LastName = "Tabor";
            myMyCar.Customer.Address = "123 E. ";
            myMyCar.Customer.DateOfPurchase = DateTime.Now;


            Console.WriteLine(myMyCar.Customer.LastName);
            Console.ReadLine();


            // call the truck subclass - you can use properties from Auto and from Truck

            Truck MyTruck = new Truck();
            MyTruck.Model = "Biggie";
            MyTruck.Maker = "Toyota";
            MyTruck.Color = "Blue";
            MyTruck.TowingCapacity = 100;
            MyTruck.Tow();
            MyTruck.Haul();

            MyTruck.Start();

            Console.ReadLine();

            //overwrite methods

            myCar.Drive(5)

        }
    }
}
