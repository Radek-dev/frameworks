﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassIntro
{
    //add class into the prject
    class Auto
    {
        //declare a properties or values the class can have to be called
        //there is a problem with this class, variable can take invalid values so,
        //it must be conditioned in a way that it the class is stable and can be used
        //by other codes
        private string make;
        public string Model;
        public int Year;
        public string Color;
        public int Miles;
        private int speed;

        //declare data type engine
        public Engine Engine;

        //declare a property of the private variable 'make', the property is public
        //however it is constrined to Oldsmobile nad Toyota only
        //set what values are allowed for make over its property

        //simplest declaration

        //public string Make
        //{

        //  return the value - that is the variable make
        // send back the value that is currently stored in the private value variable 'make'
        //    get { return make; }

        // set the value of the variable 'make' as you like - set the conditions for it here
        //    set { make = value; }
        
        //}


        //containment - this class is in the class of Auto
        //model associations

        //create a constrator for the class
        public Auto()
        {
            //Do important initializzation stuff here
            //this is called each time class auto is used in the main code
            //should initials the class in a ready state before it is ready to be used
            //take control of the initialization of the class to get it into valid state, pass in values into the constructor - can be overloaded
            Console.WriteLine("Initializing...");

        }

        //overloading - different methods, overwriting - creating additional properties

        //this can be called from the main probram - set into initial state
        public Auto(string _make, string _model)
        {
            make = _make;
            Model = _model;
        }

        //demostrating overloading - signiture for the method 'Drive' is given by the data typed bassed into the method
        //implies that the object should drive forever until it is stopped by the main code
        public void Driver()
        {

        }

        //stop after certain number of miles before stopping
        public void Drive(int miles)
        {
        }

        //should drive until it reaches the X Y coordinate
        public void Drive(int locationX, int locationY)
        {
        }

        //should drive until it reaches specific named location
        public void Drive(string location)
        {
        }



        public AutoCustomer Customer = new AutoCustomer();

        public class AutoCustomer
        {
            public string LastName;
            public string Address;
            public DateTime DateOfPurchase;
        }

        //this is a property of the make
        public string Maker
        {
            get { return make; }
            set 
            {
                switch (value)
                {
                    case "Oldsmobile":
                        make = value;
                        break;
                    case "Toyota":
                        make = value;
                        break;
                    default:
                        throw new Exception("Not a valid Maker");
                }
            }
        }


        //declare the private property
        //the property can be filtered, adjusted
        // always use the name of the property to call the value
        public int CurrentSpeed
        {
            get { return speed; }
            set
            {
                if (value < 0)
                    speed = 0;
                else if (value > 110)
                    speed = 110;
                else
                    speed = value;    
            }
        }

        //can declare a sub methods in the class as well, things you want to do with the class
        public int Accelerate(int increasedSpeed)
        {
            CurrentSpeed += increasedSpeed;
 //           Console.WriteLine("Current speed: " + Speed.ToString());
            writeLine("Current speped: " + CurrentSpeed.ToString());
            
            return CurrentSpeed;
        }

        public int Decelerate(int decreasedSpeed)
        {
            CurrentSpeed -= decreasedSpeed;
 //           Console.WriteLine("Current speed: " + Speed.ToString());

            writeLine("Current speped: " + CurrentSpeed.ToString());
            return CurrentSpeed;

        }

        //do not use two return statement, declare a method in the class
        public string SpeedLimitVialation(string initilaMessage, int speedLimit)
        {
            string message ="";

            if (CurrentSpeed > speedLimit)

                message = "Too fast";
             //   return "Too fast";
            else
                message = "You are ok";
                //return "You are OK";

            return initilaMessage + " " + message;
        }

        //helper method used only in this class
        private void writeLine(string message)
        {
           
            Console.Write("FROM MY HELPER MEHOD ... ");
            Console.WriteLine(message);
        }


        public void Start()
        {
            Console.WriteLine("Starting...");
        }

        
        //can define a class here however keep seperate files for each class
        //class Engine
        //{
        //
        //}
        //this is how to define the class
    }
}
