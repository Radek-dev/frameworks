﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Collections
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //declare the list - is not an array as you can work with as with an excel table - sorting, adding and removing values
            //list can store any value, it had one dimension only!!! - use hashtable for more dimensions
            List<string> students = new List<string>();

            //adding records, remember that there is a lot of methods and properties the List class have - it is defined by default
            students.Add("Jenny");
            students.Add("Peter");
            students.Add("Mary Jane");

            //you can sort the list;
            students.Sort();

            //you can reverse the list;
            students.Reverse();

            //you can remove an item
            students.Remove("Peter");

            //or use students.RemoveRange(0,2) - if you wann remove a range, check specs for removerange before using it

            foreach (string strName in students)
            {
                listBox1.Items.Add(strName);
            }

            //count property
            listBox1.Items.Add(students.Count);

            //add a record at any time

            students.Add("Radek");

            //remember to use the same referencing to the value in the list as you would use with an array
            //however remember that you need to handle dinamic referencing if you are about to 
            listBox1.Items.Add(students[3]);
        }
    }
}
