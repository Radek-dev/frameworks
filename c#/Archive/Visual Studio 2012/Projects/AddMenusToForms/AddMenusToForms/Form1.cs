﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddMenusToForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void mnuQuit_Click(object sender, EventArgs e)
        {
                //Yes not text box decleration
            if (MessageBox.Show("Really Quit?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void mnuCut_Click(object sender, EventArgs e)
        {
            if (textBox1.SelectedText != "")
            {
                MessageBox.Show(textBox1.SelectedText);
                textBox1.Cut();
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (textBox1.CanUndo == true)
            {
                textBox1.Undo();
                textBox1.ClearUndo();
            }
        }

        private void mnuCopy_Click(object sender, EventArgs e)
        {
            if (textBox1.SelectionLength > 0)
            {

                textBox1.Copy();

            }
        }

        private void mnuPaste_Click(object sender, EventArgs e)
        {
            //check the clipboard to see if there is any data to copy
            if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
            {

                textBox2.Paste();
                Clipboard.Clear();

            }
        }

        private void mnuViewTextboxes_Click(object sender, EventArgs e)
        {
            if (mnuViewTextboxes.Checked)
            {

                textBox1.Visible = true;
                textBox2.Visible = true;
                mnuViewTextboxes.Checked = false;

            }
            else
            {

                textBox1.Visible = !textBox1.Visible;
                textBox2.Visible = false;
                mnuViewTextboxes.Checked = true;

            }

        }

        private void mnuViewImages_Click(object sender, EventArgs e)
        {
            string Chosen_File = "";
            
            //dialog box settings and opening a chosen file - selected file

            openFD.Title = "Insert and Image";
            //note the special folder '= System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);'
            //this is for my documents
            openFD.InitialDirectory = "C:";
            openFD.Filter = "JPEG|*.jpg|GIF Images|*.gif|BITMAPS|*.bmp|All Files|*.*";


            //test to see if the the user pressed cancel button
            //check all the other operations for DialogResult
            if (openFD.ShowDialog() == DialogResult.Cancel)
            {

                MessageBox.Show("Operation Cancelled");

            }
            else
            {
                //read in the chosen file
                Chosen_File = openFD.FileName;
                //use the image
                pictureBox1.Image = Image.FromFile(Chosen_File);
            }
           

        }



        private void mnuOpen_Click(object sender, EventArgs e)
        {
            string ChosenFile = "";

            openFD.InitialDirectory = "C:";
            openFD.Title = "Open a Text File";
            openFD.FileName = "";

            openFD.Filter = "Text Files|*.txt|Word Documents|*.doc";

            if (openFD.ShowDialog() != DialogResult.Cancel) ;
            {
                ChosenFile = openFD.FileName;
                richTextBox1.LoadFile(ChosenFile, RichTextBoxStreamType.PlainText);
            }
        }

        private void mnuSave_Click(object sender, EventArgs e)
        {
            string SavedFile = "";

            saveFD.InitialDirectory = "C:";
            saveFD.Title = "Sve a Text file";
            saveFD.FileName = "";

            saveFD.Filter = "Text Files|*.txt|All Files|*.*";

            if (saveFD.ShowDialog() != DialogResult.Cancel) ;
            {
                SavedFile = saveFD.FileName;
                //this line does the saving
                richTextBox1.SaveFile(SavedFile, RichTextBoxStreamType.PlainText);
            }

        }

    }
}
