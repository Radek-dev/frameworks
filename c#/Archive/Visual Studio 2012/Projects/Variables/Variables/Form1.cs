﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Variables
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStrings_Click(object sender, EventArgs e)
        {
           //declare a string
            string strfirstName, messsageText;
            strfirstName = textBox12.Text;
            messsageText = "Your name is: ";

            //concatenate the string a print it in the label 'TextMessage'
            TextMessage.Text = messsageText + strfirstName;

            //print the string the the MessageBox on the form 
            textBox1.Text = messsageText + strfirstName;
            
            //This would create a message for the user
            //MessageBox.Show(messsageText+strfirstName, "Message", MessageBoxButtons.OK, MessageBoxIcon.Question);

            //declare an integer
            int myInteger;

            myInteger = 25;

            //use the method 'ToString' to convert the number in a string and output the string
          //  MessageBox.Show(strfirstName+myInteger.ToString());

            //declare a float
            float myFloat;

            //capital letter F on the end means Float. You can leave it off, but C# then treats it like a double. Because you've set the variable up as a float
            //be awere of the round with this data type - 7 digit precise
            myFloat = 0.42181821813848F;

          //  MessageBox.Show(strfirstName + myFloat.ToString());

            double myDouble;

            myDouble = 12345678.1234567;

            //addition is handled in the below way
            MessageBox.Show(strfirstName + (myDouble+myDouble).ToString());

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
