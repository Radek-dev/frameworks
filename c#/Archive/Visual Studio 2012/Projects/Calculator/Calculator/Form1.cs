﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//issues left are the buttons clearing the txtDisplay box after pressing equals
//the dot shot not run if there is one already there

namespace Calculator
{
    public partial class CalculatorForm : Form
    {
        public CalculatorForm()
        {
            //This part execuse as soon as the form is loaded
            InitializeComponent();
            double currentValue;

           
        }

        double total1 = 0, total2 = 0,initialValue=0;
        string strOperator;

        //the codes are only executed when the buttons are clicked
        private void btnOne_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnOne.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnTwo_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnTwo.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnThree_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnThree.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnFour_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnFour.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnFive_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnFive.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnSix_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnSix.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnSeven_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnSeven.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btn8.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnNine_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnNine.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnZero_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = txtDisplay.Text + btnZero.Text;
            initialValue = double.Parse(txtDisplay.Text);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtDisplay.Clear();
            total2 = 0;
            total1 = 0;
        }

        private void btnPoint_Click(object sender, EventArgs e)
        {
            //or use txtDisplay.Text = txtDisplay.Text + btnPoint.Text;
            //there is a mistake as the user can enter as many dots as he wishes
            txtDisplay.Text = txtDisplay.Text + ".";
        }

        private void btnEquals_Click(object sender, EventArgs e)
        { 
            if (total1 == 0)
            {

            }
            else if (total1 != 0)
            {
                //switch(strOperator)
                //{
                //    case "+":
                //        total2 = total1 + double.Parse(txtDisplay.Text);
                //        break;

                //    case "-":
                //        total2 = total1 - double.Parse(txtDisplay.Text);
                //        break;
                        
                //    case "*":
                //        total2 = total1 * double.Parse(txtDisplay.Text);
                //        break;
                        
                //    case "/":
                //        total2 = total1 / double.Parse(txtDisplay.Text);
                //        break;

                //    default:

                //        break;
                //}

                total2 = CalcOperation(total1, double.Parse(txtDisplay.Text), strOperator);


                txtDisplay.Text = total2.ToString();
                total2 = 0;
                total1 = total2;
            }
        }

       

        private void btnPlus_Click(object sender, EventArgs e)
        {
            if (total1 != 0)
            {
                total1 = CalcOperation(total1, double.Parse(txtDisplay.Text), "+");
                txtDisplay.Text = total1.ToString();
                //  total1 = initialValue * double.Parse(txtDisplay.Text);
            }
            else
            {
                total1 = double.Parse(txtDisplay.Text);
                txtDisplay.Clear();
            }
            strOperator = "+";
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            if (total1 != 0)
            {
                total1 = CalcOperation(total1, double.Parse(txtDisplay.Text), "-");
                txtDisplay.Text = total1.ToString();
                //  total1 = initialValue * double.Parse(txtDisplay.Text);
            }
            else
            {
                total1 = double.Parse(txtDisplay.Text);
                txtDisplay.Clear();
            }
            strOperator = "-";
        }

        private void btnMulti_Click(object sender, EventArgs e)
        {
            if (total1 != 0)
            {
                total1 = CalcOperation(total1, double.Parse(txtDisplay.Text), "*");
                txtDisplay.Text = total1.ToString();
                //  total1 = initialValue * double.Parse(txtDisplay.Text);
            }
            else
            {
                total1 = double.Parse(txtDisplay.Text);
                txtDisplay.Clear();
            }
            strOperator = "*";
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            if (total1 != 0)
            {
                total1 = CalcOperation(total1, double.Parse(txtDisplay.Text), "/");
                txtDisplay.Text = total1.ToString();
                //  total1 = initialValue * double.Parse(txtDisplay.Text);
            }
            else
            {
                total1 = double.Parse(txtDisplay.Text);
                txtDisplay.Clear();
            }
            strOperator = "/";
        }

        public double CalcOperation(double firstNumber, double secondNumber, string Operator)
        {
            switch (Operator)
            {
                case "+":
                    return firstNumber + secondNumber;

                case "-":
                    return firstNumber - secondNumber;

                case "*":
                    return firstNumber * secondNumber;

                case "/":
                    return firstNumber / secondNumber;

                default:
                    return 0;
            }
        }
    }
}
