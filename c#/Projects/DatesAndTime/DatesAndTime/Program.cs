﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatesAndTime
{
    class Program
    {
        static void Main(string[] args)
        {
            //stores both date and time at the same moment
            //
            DateTime myValue = DateTime.Now;

            //display whole info
            //Console.WriteLine(myValue.ToString());

            //display only date
            Console.WriteLine(myValue.ToShortDateString());

            Console.WriteLine(myValue.ToShortTimeString());

            //seconds also
            Console.WriteLine(myValue.ToLongTimeString());

            //MODIFY THE DATES

            //adding 3 days
            Console.WriteLine(myValue.AddDays(3).ToLongDateString());

            //just the months
            Console.WriteLine(myValue.Month.ToString());

            //overloading constructors

            // new date
            DateTime myBirthday = new DateTime(1985, 12, 7);

            Console.WriteLine(myBirthday.ToShortDateString());

            //you should catch this with try parse
            myBirthday = DateTime.Parse("25/02/1985");

            //subtracting two times
            //requires specific data type TimeSpan to hold the difference
            TimeSpan myAge = DateTime.Now.Subtract(myBirthday);

            Console.WriteLine(myAge.TotalDays);

            Console.ReadLine();

        }
    }
}
