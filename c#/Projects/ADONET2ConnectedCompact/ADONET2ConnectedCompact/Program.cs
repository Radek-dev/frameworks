﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;


//video 5 day 8
//can't use the library - not sure why

//It turns out VS2013 dropped the .sdf format.

//Is Microsoft dropping support for SDF database files in Visual Studio?
//http://stackoverflow.com/questions/18598506/is-microsoft-dropping-support-for-sdf-database-files-in-visual-studio

//The answer is yes: Microsoft is silently dropping support (as usual IMHO) to Sql Compact Edition.

//It started abandoning Sql CE 3.5 in Vs2012 It continued dropping Sql CE in Sql Management Studio 2012 and finally in VS2013

//You can use CompactView or install SQL Server Compact Toolbox extension in VS2013 or (my prefered solution) use Sql management Studio 2008

//UPDATE thanks to Nicolas' comment

//As stated by Microsoft:

//SQL Server compact edition is in deprecation mode with no new releases planned near future. 
//Last release SQL CE 4.0SP1 (and earlier releases that are still in the support cycle) will continue
//to be supported through its lifecycle and Microsoft is committed to fix any major, production blocking issues found in these releases.

namespace ADONET2ConnectedCompact
{
    class Program
    {
        static void Main(string[] args)
        {
            // Step 1: Handshake with the database
            // this is the connection string
            SqlCeConnection myConnection = new SqlCeConnection("Data Source=Database1.sdf");

            myConnection.Open();

            // Step 2: Specify a command (request)
            //this is sanding the command for the database
            //Table Direct is the same as the select query or one can use stored procedure
            SqlCeCommand myCommand = myConnection.CreateCommand();
           
            myCommand.CommandText = "Customers";
            myCommand.CommandType = System.Data.CommandType.TableDirect;

            SqlCeDataReader myDataReader;

            myDataReader = myCommand.ExecuteReader();


            // Step 3: Use the data

            while (myDataReader.Read())
            {
                Console.WriteLine(myDataReader[0].ToString() + " " + myDataReader[1] + " " + myDataReader[2]);
            }

            myConnection.Close();

            Console.ReadLine();

        }
    }
}
