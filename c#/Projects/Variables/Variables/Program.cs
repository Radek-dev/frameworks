﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            //these are integers
            int x;
            int y;

            x = 7;
            y = x + 3;

            Console.WriteLine(y);
             */

            //string myFirstName;
            //myFirstName = "Bob";

            // or write
            // string myFirstName = "Bob";

            // or var
            //var MyString = "Bob";

            //Console.WriteLine(MyString);


            int x = 7;
            string y = "5";

            //convert into integer
            int mySecondTry = x + int.Parse(y);

            Console.WriteLine(mySecondTry);
            Console.ReadLine();

            //converst into string
            string myThirdTry = x.ToString() + y;
            Console.WriteLine(myThirdTry);
            Console.ReadLine();


        }
    }
}
// remember data conversions for different data types