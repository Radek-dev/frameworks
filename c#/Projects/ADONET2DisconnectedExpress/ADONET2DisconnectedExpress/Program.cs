﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//remember these!
using System.Data;
using System.Data.SqlClient;

//DataSet is the memory representation of the data

namespace ADONET2DisconnectedExpress
{
    class Program
    {
        static void Main(string[] args)
        {
            //connection string - work with it
            
            SqlConnection myConnection = new SqlConnection("Data Source=.\\SQLEXPRESS;" +
                           "AttachDbFilename=|DataDirectory|\\Database1.mdf; Integrated Security = True; User Instance=True");
            myConnection.Open();

            SqlDataAdapter myDataAdapter = new SqlDataAdapter();

            myDataAdapter.SelectCommand = myConnection.CreateCommand();
            myDataAdapter.SelectCommand.CommandText = "SELECT * FROM Customers";

            myDataAdapter.InsertCommand = myConnection.CreateCommand();
            myDataAdapter.InsertCommand.CommandText = "INSERT INTO Customers (FirstName, LastName, City, State) " +
                                                                    "VALUES (@FirstName, @LastName, @City, @State)";
            //how to match the values with input paramaters
            //data collection
            myDataAdapter.InsertCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50, "FirstName");
            myDataAdapter.InsertCommand.Parameters.Add("@LastName", SqlDbType.NVarChar, 50, "LastName");
            myDataAdapter.InsertCommand.Parameters.Add("@City", SqlDbType.NVarChar, 50, "City");
            myDataAdapter.InsertCommand.Parameters.Add("@State", SqlDbType.NVarChar, 50, "State");


            //UPDATE Command
            myDataAdapter.UpdateCommand = myConnection.CreateCommand();
            myDataAdapter.UpdateCommand.CommandText = "UPDATE Customers " +
                                                      "SET FirstName=@FirstName, LastName = @LastName, City=@City, State=@State " +
                                                      "WHERE CustomerID = @CustomerID";
            myDataAdapter.UpdateCommand.Parameters.Add("@CustomerID", SqlDbType.Int, 4, "CustomerID");
            myDataAdapter.UpdateCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50, "FirstName");
            myDataAdapter.UpdateCommand.Parameters.Add("@LastName", SqlDbType.NVarChar, 50, "LastName");
            myDataAdapter.UpdateCommand.Parameters.Add("@City", SqlDbType.NVarChar, 50, "City");
            myDataAdapter.UpdateCommand.Parameters.Add("@State", SqlDbType.NVarChar, 50, "State");


            //delete command

            myDataAdapter.DeleteCommand = myConnection.CreateCommand();
            myDataAdapter.DeleteCommand.CommandText = "DELETE FROM Customers WHERE CustomerID = @CustomerID";
            myDataAdapter.DeleteCommand.Parameters.Add("@CustomerID", SqlDbType.Int, 4, "CustomerID");

           

            DataSet myDataSet = new DataSet();
            myDataAdapter.Fill(myDataSet);

            myConnection.Close();

            Console.WriteLine("BEFORE");
            foreach (DataRow myRow in myDataSet.Tables[0].Rows)
            {
                Console.WriteLine(myRow["FirstName"] + " " + myRow["LastName"]);
            }

            //Make some modifications to the DataSet's data

            // Insert

            DataRow newDataRow = myDataSet.Tables[0].NewRow();
            newDataRow["FirstName"] = "Brian";
            newDataRow["LastName"] = "Faley";
            newDataRow["City"] = "Denver";
            newDataRow["State"] = "CO";

            //
            myDataSet.Tables[0].Rows.Add(newDataRow);

            // Update

            myDataSet.Tables[0].Rows[0]["FirstName"] = "Robert";

            Console.WriteLine("Row State 1: " + myDataSet.Tables[0].Rows[0].RowState);
            Console.ReadLine();
            Console.WriteLine("Row State 3: " + myDataSet.Tables[0].Rows[2].RowState);
            Console.ReadLine();

           // myDataSet.Tables[0].AcceptChanges();

            Console.WriteLine("Row State 1: " + myDataSet.Tables[0].Rows[0].RowState);
            Console.ReadLine();
            Console.WriteLine("Row State 3: " + myDataSet.Tables[0].Rows[2].RowState);
            Console.ReadLine();

            // Delete

            //myDataSet.Tables[0].Rows[1].Delete();

            // Connect back up, commit the changes
            //  It will connect on it's own, no need to explicitly re-open the connection
            // http://stackoverflow.com/questions/6833277/dataadapter-update-does-not-update-the-database

            SqlCommandBuilder cb = new SqlCommandBuilder(myDataAdapter);

            myDataAdapter.DeleteCommand = cb.GetDeleteCommand(true);
            myDataAdapter.UpdateCommand = cb.GetUpdateCommand(true);
            myDataAdapter.InsertCommand = cb.GetInsertCommand(true);

            Console.WriteLine("Row State 1: " + myDataSet.Tables[0].Rows[0].RowState);
            Console.ReadLine();
            Console.WriteLine("Row State 3: " + myDataSet.Tables[0].Rows[2].RowState);
            Console.ReadLine();

            myDataAdapter.AcceptChangesDuringUpdate = true;
            myDataAdapter.AcceptChangesDuringFill = true;

           // myDataSet.Tables[0].AcceptChanges();
            
            myDataAdapter.Update(myDataSet);

            Console.WriteLine("Row State 1: " + myDataSet.Tables[0].Rows[0].RowState);
            Console.ReadLine();
            Console.WriteLine("Row State 3: " + myDataSet.Tables[0].Rows[2].RowState);
            Console.ReadLine();

            Console.WriteLine("AFTER");
            foreach (DataRow myRow in myDataSet.Tables[0].Rows)
            {
                Console.WriteLine(myRow["FirstName"] + " " + myRow["LastName"]);
            }


            Console.ReadLine();
        }

        /*
         * from the first example
         //connection string - work with it
            
            SqlConnection myConnection = new SqlConnection("Data Source=.\\SQLEXPRESS;" +
                           "AttachDbFilename=|DataDirectory|\\Database1.mdf; Integrated Security = True; User Instance=True");
            myConnection.Open();


            SqlCommand myCommand = myConnection.CreateCommand();
            myCommand.CommandType = CommandType.Text;
            myCommand.CommandText = "SELECT * FROM Customers";

            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);

            DataSet myDataSet = new DataSet();
            myDataAdapter.Fill(myDataSet);

            myConnection.Close();


            foreach (DataRow myRow in myDataSet.Tables[0].Rows)
            {
                Console.WriteLine(myRow["FirstName"] + " " + myRow["LastName"]);
            }

            Console.ReadLine();
         
        */
    }
}
