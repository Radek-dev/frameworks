﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            //this is the object of the class 'Car'
            //this calls the class
            //new instance of a class
            Car myNewCar = new Car();

            //assign new value of the properties
            myNewCar.Make = "Oldsmobile";
            myNewCar.Mode = "Culas Supreme";
            myNewCar.Year = 1986;
            myNewCar.Color = "blue";


            //write out the properties
            Console.WriteLine("{0} - {1} - {2} - {3}",
                    myNewCar.Make,
                    myNewCar.Mode,
                    myNewCar.Year,
                    myNewCar.Color);
            Console.WriteLine("Car's value: {0:C}", myNewCar.DetermineMarketValue());

            Console.ReadLine();

            //call a method
            double marketValueOfCar = determineMarketValue(myNewCar);

            Console.WriteLine(marketValueOfCar);

            //or use this to declare the market value, 'C' for currency

        }

        //local method, uses Car a pass in parameter
        private static double determineMarketValue(Car _car)
        {
            double carValue = 100.00;

            //Some day write a code to go online and look up the car's value
            //and trieve its value into the carValue variable
            return carValue;
        }


    }

    // what kind of code block we are creating - use class
    class Car
    {
        //write the memebers of this class, I think it is methods and properties
        //properties as below
        public string Make { get; set; }
        public string Mode { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }

        //method as class member
        public double DetermineMarketValue ()
        {
            double carValue = 100.0;

            //this referes to current instance of class, all properties
            //this means part of class implementation
            if (this.Year > 1990)
                carValue = 10000.0;
            else
                carValue = 2000.0;

            return carValue;
        }

    }


}
