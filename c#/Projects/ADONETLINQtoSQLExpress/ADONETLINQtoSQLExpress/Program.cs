﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//All that is needed is here:
//      http://stackoverflow.com/questions/8416301/how-to-connect-to-sql-server-express-db-from-server-explorer-vs-2010
//and here:
//      http://blogs.msdn.com/b/charlie/archive/2007/11/19/connect-to-a-sql-database-and-use-the-sql-designer.aspx  

//REMEMBER that this is LINQtoEntities approach

namespace ADONETLINQtoSQLExpress
{
    class Program
    {
        static void Main(string[] args)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            //var query = from c in db.Customers
            //            where c.City == "Dalls"
            //            select c.City;

            //var query = from c in db.Customers
            //            select c.FirstName;

            //pulls back entity set of customers - changed into T-SQL to return the relevant data
            //filter as much as you can to return only relevant data column - only the rows that are needed
            //query is not executed until we needed in the code
            var query = db.Customers;


            /*
            var customers = from c in db.Customers
                            select c;


            //remember that this sends two queries to the database - one for each line of code
            foreach (var q in customers)
            {
                Console.WriteLine(q.LastName);
            }
            
             */
            //Simple query, project IReadOnlyCollection column needed

            // var customers = from c in db.Customers
            //                select c.LastName;

            //or use method syntax

            //var customers = db.Customers.Select(c => c.LastName);

            ////remember that this sends two queries to the database - one for each line of code
            //foreach (var q in customers)
            //{
            //    Console.WriteLine(q);
            //}

            //select multiple columns
            //var customers = from c in db.Customers
            //                select new { c.LastName, c.FirstName };

            //var customers = db.Customers.Select(c => new { c.LastName, c.FirstName });

            //WHERE
            //var customers = from c in db.Customers
            //                where c.State == "IL"
            //                select c;

            //Method syntax
            //var customers = db.Customers.Where(c => c.State == "IL");

            ////remember that this sends two queries to the database - one for each line of code
            //foreach (var q in customers)
            //{
            //    Console.WriteLine(q.LastName);
            //}

            // Project properties from EntityCollection Entities - SHAPED results

            //return just a single value
            // or return default
            //var customer = (from c in db.Customers
            //               where c.CustomerID == 4
            //               select c).FirstOrDefault();

            //if (customer != null)
            //Console.WriteLine(customer.LastName);

            //foreach (var q in customer)
            //{
            //    Console.WriteLine(q);
            //}

            //UPDATING DATA
            //http://msdn.microsoft.com/en-us/library/bb386931(v=vs.110).aspx

            var cust = (from c in db.Customers
                       where c.CustomerID == 1
                       select c).First();

            Console.WriteLine(cust.FirstName);

            Customer newCust = new Customer();

            newCust.FirstName = "Brian";
            newCust.LastName = "Faley";
            newCust.City = "Danver";
            newCust.State = "CO";

            //This is how to add a line to the databse
            db.Customers.InsertOnSubmit(newCust);
            db.SubmitChanges();
            

            //This is how to change the record cust 
            cust.FirstName = "Bob";

            var newCustToDelete = db.Customers.Where(c => c.FirstName == "Brian").First();

            Console.WriteLine(newCustToDelete.CustomerID);
            //REMEBER to use if not null with these - firstorDefalt - it may run into errors easily
            //to delate
            var custToDelete = (from c in db.Customers
                               where c.FirstName == "Brian"
                               select c).First();

            db.Customers.DeleteOnSubmit(custToDelete);

            //This is how to commit the changes to the database
            db.SubmitChanges();

            Console.WriteLine(cust.FirstName);

            Console.ReadLine();
        }

    }
}
