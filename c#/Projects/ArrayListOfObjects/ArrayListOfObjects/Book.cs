﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayListOfObjects
{
    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }

        public void Print()
        {
            Console.WriteLine("\"{0}\", {1}, (ISBN: {2})", Title, Author, ISBN);
        }
    }
}
