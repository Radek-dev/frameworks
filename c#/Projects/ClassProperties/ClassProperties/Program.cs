﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassProperties
{
    //notes: http://msdn.microsoft.com/en-us/library/x9fsa0sw.aspx

    public partial class Dog
    {
        //properties hold values
        //this declaration is a short hand notation
       // public string Name { get; set; }

        //full hand is:
        //this private member is backing store
        // -> how the value is stored in the memory
        //allows us to do additional actions when
        //interacting with the property name
        private string _name;

        public string Name
        {
            get { return _name; }
            set { 
                //look at the value
                //validate
                _name = value; }
        }
        
        //METHOD but returns nothing
        //set default value buy setting what = "bark" as default
        //overlaoding - method signiture
        public void Speak(string what = "bark")
        {
            //TODO here

            //Caling the event
            //It is called when ever Speak runs
        //nobody subcribe to it
            if (HasSpoken != null)
                HasSpoken(this, EventArgs.Empty);
        }

        //EVENTS
        //event handler
        public event EventHandler HasSpoken;

    }

    public class Trainer
    {
        void Operate()
        {
            var dog = new Dog();

            //subcribing to an EVENT to 'HasSpoken'
            //'+=' means 'add to', short hand
            dog.HasSpoken += dog_HasSpoken;
        }

        void dog_HasSpoken(object sender, EventArgs e)
        {
            //This is the event, the code that will run
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
      
        }

    }
}
