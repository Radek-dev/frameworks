﻿//using shortens the coding
//contains all the class we tend to use
//using this makes not difference to complier
//can clean up the code, more expresive

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tabor;

//NAMESPACE
//means something like surname for each class in .NET Library - removes the abiguity
//one Namespace ca contain other namespaces - usually pretty short - 2 namespaces
//ADDING A REFERENCE TO OTHER ASSAMPLIES - OR LIBRARIES
//Namespaces = way to reference class

namespace NamespaceAndReferencingAssemblies
{
    class Program
    {
        static void Main(string[] args)
        {
            //referencing with full name
            //System.IO.StreamReader myStreamReader = new System.IO.StreamReader();

            //StreamReader myStreamReader = new StreamReader();

            Bob bob = new Bob();

            string html = bob.Lookup("http://www.learnvisualstudio.net");

            Console.WriteLine(html);
            Console.ReadLine();

        }
    }
}
