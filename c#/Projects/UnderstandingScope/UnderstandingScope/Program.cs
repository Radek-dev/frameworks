﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//PRIVATE X PUBLIC
//PRIVATE means it can be called inside the class - one block of code
//PUBLIC means used by by other blocks of code
//The issue is about accessibility


namespace UnderstandingScope
{
    class Program
    {
        //if use private - it can't be used outside Program
        private static string k = "";

        static void Main(string[] args)
        {
            string j = "";

            for (int i = 0; i < 10; i++)
            {
                j = i.ToString();
                k = i.ToString();
                Console.WriteLine(i);
            }

            //Console.WriteLine(i);
            Console.WriteLine("Outsite of the for: " + j);
            Console.WriteLine("k: " + k);
            Console.ReadLine();

            Car myCar = new Car();

            //CHECK THIS
            //can access only the public method
            myCar.DoSomething();

            Console.ReadLine();

        }

        //still access the static string here
        static void helperMethod()
        {
            Console.WriteLine("k: "+k);
        }

    }

    class Car
    {   //CHECK this
        //public starts with capitals
        public void DoSomething()
        {
            Console.WriteLine(helperMethod());
        }

        //private starts with no capitals
        private string helperMethod()
        {
            return "Hello World";
        }
    }
}
