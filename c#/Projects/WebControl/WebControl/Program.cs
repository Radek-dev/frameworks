﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

//http://stackoverflow.com/questions/1131425/send-a-file-via-http-post-with-c-sharp

namespace WebControl
{
    class Program
    {
        static void Main(string[] args)
        {

            int x = 2;

            if (x==1)
            {
                string url = "http://www.learnvisualstudio.net";

                WebRequest webRequest = WebRequest.Create(url);

                webRequest.Credentials = CredentialCache.DefaultCredentials;

                //http://msdn.microsoft.com/en-us/library/system.net.httpwebresponse(v=vs.110).aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-1
                HttpWebResponse httpWebResponse = (HttpWebResponse)webRequest.GetResponse();

                Console.WriteLine(httpWebResponse.StatusDescription);

                Stream responseStream = httpWebResponse.GetResponseStream();

                StreamReader streamReader = new StreamReader(responseStream);

                //Console.WriteLine(streamReader.CreateObjRef());

                string result = streamReader.ReadToEnd();
                streamReader.Close();
                responseStream.Close();
                httpWebResponse.Close();


                Console.WriteLine(result);
                Console.ReadLine();

                //WebClient 
                //http://msdn.microsoft.com/query/dev12.query?appId=Dev12IDEF1&l=EN-US&k=k(System.Net.WebClient);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv4.5);k(DevLang-csharp)&rd=true
                //http://msdn.microsoft.com/en-us/library/ms144229(v=vs.110).aspx  
            }
            else
            {
                string url = "http://www.sdf.org.uk";

                WebRequest webRequest = WebRequest.Create(url);

                webRequest.Credentials = CredentialCache.DefaultCredentials;

                //http://msdn.microsoft.com/en-us/library/system.net.httpwebresponse(v=vs.110).aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-1
                HttpWebResponse httpWebResponse = (HttpWebResponse)webRequest.GetResponse();

                Console.WriteLine(httpWebResponse.StatusDescription);

                Stream responseStream = httpWebResponse.GetResponseStream();

                StreamReader streamReader = new StreamReader(responseStream);

                //Console.WriteLine(streamReader.CreateObjRef());

                string result = streamReader.ReadToEnd();
                streamReader.Close();
                responseStream.Close();
                httpWebResponse.Close();


                Console.WriteLine(result);

                //WebClient 
                //http://msdn.microsoft.com/query/dev12.query?appId=Dev12IDEF1&l=EN-US&k=k(System.Net.WebClient);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv4.5);k(DevLang-csharp)&rd=true
                //http://msdn.microsoft.com/en-us/library/ms144229(v=vs.110).aspx

                try
                {
                    WebClient client = new WebClient();
                    client.UploadFile(url, "TestUpload.txt");
                }
                catch (Exception e)
                {

                    Console.WriteLine("Something didn't work : {0}", e.GetBaseException()); 
                }

                Console.ReadLine();
            } 
           
        }
    }
}
