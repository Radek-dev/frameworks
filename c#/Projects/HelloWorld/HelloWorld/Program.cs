﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace as another way of organising code
//code must be in a namespace and a class to executable
namespace HelloWorld
{
    //this is a class named Program - way to orginise the code into different classes
    class Program
    {
        //main gets executed first when the application is run
        //this is a method with name Main - can be called
        static void Main(string[] args)
        {
            /*
             
            //this is just hello world lines for the console
            //console is a class, writeline is a method
            //dot operator is 'member accessor'
            //paranteses marks a method, the info in the brakets is passed into the method
              
            */
            Console.WriteLine("Hello World!");
            
            Console.ReadLine();
           

            //green in green, blue special world, string literal are in red

            //solution explorer - solutions own  the projects

            //csproj files contains all the configurations settings for the project

            //bin stands for binary - complied versions - able to run on computer

            //debug files - are related to debugging - not optimised for a realise


        }
    }
}
