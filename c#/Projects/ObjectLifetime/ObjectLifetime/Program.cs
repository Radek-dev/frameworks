﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectLifetime
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //create a spot for the in memory to hold the whole object, instance
            //holding the reference to the memory via this structure - names of objects
            Car myCar = new Car();

            //set properties

            //another instance
            //if not holding the pointer to the memory, the data is cleared
            // this is called constructor of an object
            Car myOtherCar = new Car("Ford", "Escape", 2055, "white");

            //this wipes the memory, removes it from memory
            myCar = null; myOtherCar = null;


            //the methods is there already
            Car.MyMethod();

        }
    }

    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        public double OriginalPrice { get; set; }

        public Car()
        {
            //you could this from a configuration file, a database, etc.
            //I will just hard code in this instance.
            this.Make = "Nissan";
        }
        // this is constructor
        public Car(string make, string model, int year, string color)
        {
            Make = make;
            Model = model;
            Year = year;
            Color = color;
        }

        //will be there without creating the instance of the class - it is static
        //'static' used for classes or methods to they are declared and called when ever the object is used
        //static methods and classes are available without calling/establishing an instante of a class first
        public static void MyMethod();

    }
}
