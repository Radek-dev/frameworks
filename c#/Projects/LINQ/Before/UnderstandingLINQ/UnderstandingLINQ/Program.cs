﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// help: http://code.msdn.microsoft.com/101-LINQ-Samples-3fb9811b

namespace UnderstandingLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Car> myCars = new List<Car>() {
                new Car() { Make="BMW", Model="550i", Color=CarColor.Blue, StickerPrice=55000, Year=2009 },
                new Car() { Make="Toyota", Model="4Runner", Color=CarColor.White, StickerPrice=35000, Year=2010 },
                new Car() { Make="BMW", Model="745li", Color=CarColor.Black, StickerPrice=75000, Year=2008 },
                new Car() { Make="Ford", Model="Escape", Color=CarColor.White, StickerPrice=28000, Year=2008 },
                new Car() { Make="BMW", Model="550i", Color=CarColor.Black, StickerPrice=57000, Year=2010 }
            };


            // We'll add code here!

            foreach (var car in myCars)
            {
                Console.WriteLine("{0} {1} - {2}", car.Make, car.Model, car.Year);
            }

            // like writing the sql queries - you can use the filter clauses
            // set the 'var' data type - figure it out yourself computer
            // this is the LINQ operation
            // we do not know the class here so you use 'var'
            var bmws = from car in myCars
                       where car.Make == "BMW"
                       && car.Year == 2009
                       select car;

            foreach (var car in bmws)
            {
                Console.WriteLine("{0} {1} - {2}", car.Make, car.Model, car.Year);
            }

            //orderding the cars by the date
            var orderCars = from car in myCars
                            orderby car.Year
                            select car;

            foreach (var car in orderCars)
            {
                Console.WriteLine("{0} {1} - {2}", car.Make, car.Model, car.Year);
            }


            //same result using methods as LINQ
            //'OrderByDescending' or 'sum' can be used as well
            //var _orderedCars = myCars.OrderByDescending(p => p.Year);

            var _bmws = myCars.Where(p => p.Year == 2010).Where(p => p.Make == "BMW");

            foreach (var car in _bmws)
            {
                Console.WriteLine("{0} {1} - {2}", car.Make, car.Model, car.Year);
            }


            Console.ReadLine();

        }
    }

    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public double StickerPrice { get; set; }
        public CarColor Color { get; set; }
    }

    enum CarColor
    {
        White,
        Black,
        Red,
        Blue,
        Yellow
    }

}
