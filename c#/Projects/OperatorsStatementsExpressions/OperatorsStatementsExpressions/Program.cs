﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsStatementsExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y;

            //Assignment operator
            x = 3;
            y = 2;

            //There are many mathematical operators.. +, -, *, /

            // comparison or evaluation operators ==, >, >=,
            // conditional operator && = "AND"
            // conditional "OR" = || - the two pipes

            // Also, here is the in-line conditional operator
            string message = (x == 1) ? "Car" : "Boat";

            //Member access and Method invocation

            Console.WriteLine("Hi");

            //Declaration Statement
            int z;

            //Expression Statement
            //myString = myFirstName + myLastName;



        }
    }
}
