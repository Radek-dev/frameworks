﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaInterfaces
{
    /// <summary>
    /// INTERFACE (= a contract)
    /// This is how you implement an interface - as a class
    /// 'I' at the begining of the interface name stands for an interface
    /// 'able' at the end - able to print
    /// can inherit only from one class
    /// </summary>
    interface IPrintable
    {
        void Print();
    }
}
