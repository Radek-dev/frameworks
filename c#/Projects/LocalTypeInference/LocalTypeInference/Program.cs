﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//var should be used in a way we determine the data type in debug model
//and use the right data type if we figure it out well


namespace LocalTypeInference
{
    class Program
    {
        static void Main(string[] args)
        {
            var first = 7; //Infers type int
            Console.WriteLine("First variable is of type: {0}", first.GetType());

            var second = "Hello world!";
            Console.WriteLine("Second variable is of type: {0}", second.GetType());
            var third = second;
            Console.WriteLine("Third variable is of type: {0}", third.GetType());

            MyHelperMethod();

            Console.WriteLine("");
            Console.WriteLine("Press enter key to contineu...");
            Console.ReadLine();
        }
        //not working
        //var myPrivateField = 1;
        //var can't be used with methods, as return values of as a parameter for a method



        static public void MyHelperMethod()
        {
            //var sixth = "Bob";

            //var seventh = null;


            Console.WriteLine("");
            Console.WriteLine("States that start with 'i' ...");

            string[] states = new string[] { "Texas", "Illinois", "New York", "Florida", "Washington", "Indiana", "Oregon", "Idaho", "Delaware", "Iowa" };

            // We don't have to try and figure out what type is being 
            // returned from this query.  Ok, this is a fairly mundane
            // example, but I assure you, it gets A LOT more convoluted!
            var iStates = from s in states
                          where s.StartsWith("I")
                          select s;

            // Here again, we don't need to know what data type is inside of iStates
            foreach (var state in iStates)
                Console.WriteLine(state.GetType());


        }

    }
}
