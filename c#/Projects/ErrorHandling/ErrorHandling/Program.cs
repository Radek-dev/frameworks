﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            
            
            //this catches errors with the code - runtime errors
            try
            {
                //new StreamReader have the exceptions
                StreamReader myReader = new StreamReader("Values.txt");

                string line = "";


                while (line != null)
                {
                    line = myReader.ReadLine();
                    if (line != null)
                        Console.WriteLine(line);
                }


                    myReader.Close();
                }
            //do this in case of error
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine("Couldn't find the directory:{0}. The Directory does not exists", e.Message);

            }

            catch (FileNotFoundException e)
            {
                Console.WriteLine("Couldn't find the file");
            }
                //this exeption will kick in if nothing else works
            catch (Exception e)
            {
                Console.WriteLine("Something didn't work : {0}", e.Message); //refer to all the details of the exeption - enumerations for it
            }

                //if you need to close connections
            finally
            {
                // Perform any cleanup to roll back the data or close connections
                // to files, database, network, etc.

                // myReader.Close();
            }
                Console.ReadLine();
        }
        
    }
}