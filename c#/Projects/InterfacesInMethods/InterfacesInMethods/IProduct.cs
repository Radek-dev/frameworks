﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    public interface IProduct
    {
        // you have to agree to implement GetStorageLocation to implement this Interface
        void GetStorageLocation();
    }
}
