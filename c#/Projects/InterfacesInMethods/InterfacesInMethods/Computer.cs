﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    // can add the interfaces
    // remember the blue small line (like missing namespace)
    // in video VCS2010_05_07 - Visual Studio IDE Overview Day 5 Implementing Interfaces, Defining Regions
    class Computer : IPrintable, IProduct
    {
        public void Print()
        {
            // this should be implemented
            throw new NotImplementedException();
        }

        public void GetStorageLocation()
        {
            throw new NotImplementedException();
        }
    }
}
