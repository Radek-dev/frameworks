﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    // this is a static class - do not have to create an instance of the class
    static class Print
    {
        // toConsole method
        // takes IPrintable variable
        public static void ToConsole(IPrintable printableObject)
        {
            printableObject.Print();
        }

    }
}
