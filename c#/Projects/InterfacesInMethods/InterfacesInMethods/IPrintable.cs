﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    /// <summary>
    /// Any one who establishes the inheritance from this interface,
    /// must use method Print in the class declaration
    /// </summary>
    interface IPrintable
    {
        void Print();
    }
}
