﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    public class Warehouse
    {
        
        // generic class called Queue
        // FIFO methodology
        // inventory is collectin of type queue of type IProduct
        private Queue<IProduct> inventory { get; set; }

        public Warehouse()
        {
            inventory = new Queue<IProduct>();
        }

        public IProduct GetOldestItem()
        {
            // first() return the first itme added to the Queue
            IProduct oldestItem = inventory.First();

            return oldestItem;
        }

        public IProduct GetNewestItem()
        {
            // Last() returns the last item added to the queue
            IProduct newestItem = inventory.Last();

            return newestItem;
        }

        /// <summary>
        /// only takes IProduct
        /// 
        /// </summary>
        /// <param name="productToAdd"></param>
        public void Add(IProduct productToAdd)
        {
            // this method adds to the queue
            inventory.Enqueue(productToAdd);
        }
        
    }
}
