﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaInheritance
{
    class Program
    {
        static void Main(string[] args)
        {

            // POLYMORHPISM = many different things
            // as long as you will try to add an item that is inheriting from
            //     Product, you can add it to the generic list

            List<Product> inventory = new List<Product>();

            Automobile a1 = new Automobile();
            a1.Make = "Dodge";
            a1.Model = "Dart";
            a1.Year = 1976;
            a1.ExteriorColor = "Green";
            a1.Miles = 111001;

            Book b1 = new Book();
            b1.ProductID = 2;
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";

            inventory.Add(a1);
            inventory.Add(b1);

            // can loop over it - you are going to implement Print
            // if the print is not in child class, use the base class
            // two different type to be added to the same colllection
            // both ingerit from the same base class
            Console.WriteLine("We have the following items in stock:");
            foreach (Product item in inventory)
            {
                item.Print();
                
            }

            Console.ReadLine();

            // Cool, but what if we don't want to provide an implementation?

        }
    }
}
