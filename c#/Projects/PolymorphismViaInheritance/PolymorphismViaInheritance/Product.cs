﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaInheritance
{
    class Product
    {
        public int ProductID { get; set; }

        // virtual means that use the specific implematation on the derivied class
        public virtual void Print()
        {
            Console.Write("ID: {0} ... ", ProductID );
        }
    }
}
