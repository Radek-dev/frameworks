﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaInheritance
{
    // inherites from Product
    class Automobile : Product 
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public int Miles { get; set; }
        public string ExteriorColor { get; set; }

        public override void Print()
        {
            base.Print();
            Console.WriteLine("{0} {1} {2} with {3} exterior (Odometer: {4})", Year, Make, Model, ExteriorColor, Miles);
        }
    }
}
