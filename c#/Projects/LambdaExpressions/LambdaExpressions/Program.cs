﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LambdaExpressions
{
    class Program
    {
        // Third try, this time with LAMBDA EXPRESSIONS.

        // Step 1: Sames as before.
        public delegate bool FunctionForString(string s);


        // Step 2: Same as before.
        public static string[] PerformOperationOnStringArray(string[] myStrings, FunctionForString operation)
        {
            System.Collections.ArrayList myList = new System.Collections.ArrayList();
            foreach (string s in myStrings)
            {
                if (operation(s))
                {
                    myList.Add(s);
                }
            }
            return (string[])myList.ToArray(typeof(string));
        }


        // Step 3: Same as our Anonymous Method example.  No need for a named method
        // that serves as the container for the algorithm.


        // Step 4: This time instead of a a full anonymous method declaration like this:
        //
        //               delegate(string s) { return s.StartsWith("A"); }
        //
        // ... instead, we use a shortcut:
        //
        //               (s => s.StartsWith("A"))
        //
        // A Lambda Expression is simply shorthand for an anonymous method.  Just learn
        // the shorthand syntax and that's all you need to know.
        // The first 's' is the input parameter.  The arrow => is just a separator.
        // The stuff after the arrow is the algorithm. 
        static void Main(string[] args)
        {
            string[] myStrings = { "Adam", "Alan", "Bob", "Steve", "Jim", "Aiden" };

                        //read this as: given 's' as an input parameter, evaluate 's', if it starts with A, return true
            string[] stringsA = PerformOperationOnStringArray(myStrings, (s => s.StartsWith("A")));

            foreach (string s in stringsA)
                Console.WriteLine(s);

            Console.ReadLine();

        }

    }
}
