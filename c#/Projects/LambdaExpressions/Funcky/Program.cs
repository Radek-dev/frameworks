﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Funcky
{
    class Program
    {
        // Let's have a little fun ...  

        // Step 1: What if we do what we said originally and made this
        // more generic?  So, instead of this:
        //
        // public delegate bool FunctionForString(string s);
        // 
        // ... we do this:
        //public delegate bool FunctionForAnything<T>(T item);

        // Step 2: That change requires us to modify the method signature a bit:
        //public static string[] PerformOperationOnStringArray(string[] myStrings, FunctionForAnything<string> operation)
        //{
        //    System.Collections.ArrayList myList = new System.Collections.ArrayList();
        //    foreach (string s in myStrings)
        //    {
        //        if (operation(s))
        //        {
        //            myList.Add(s);
        //        }
        //    }
        //    return (string[])myList.ToArray(typeof(string));
        //}


        public static string[] PerformOperationOnStringArray(string[] myStrings, Func<string, bool> operation)
        {
            System.Collections.ArrayList myList = new System.Collections.ArrayList();
            foreach (string s in myStrings)
            {
                if (operation(s))
                {
                    myList.Add(s);
                }
            }
            return (string[])myList.ToArray(typeof(string));
        }



        // That doesn't seem like such a big deal. but it allows us to re-use that 
        // function type declaration (return bool, accept one parameter) many
        // times.
        //
        // THAT is exactly what Microsoft has done with the Func delegate declarations
        // that you see in the method signatures for LINQ query expression parameters.
        // We'll come back to the idea when we get to the LINQ query expressions later
        // in this series.



        static void Main(string[] args)
        {
            string[] myStrings = { "Adam", "Alan", "Bob", "Steve", "Jim", "Aiden" };

            string[] stringsA = PerformOperationOnStringArray(myStrings, (s => s.StartsWith("A")));

            string[] stringsB = PerformOperationOnStringArray(myStrings, (s => s.StartsWith("B")));

            foreach (string s in stringsA)
                Console.WriteLine(s);

            foreach (string s in stringsB)
                Console.WriteLine(s);

            Console.ReadLine();
        }
    }
}

