﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesPart1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] myStrings = { "Adam", "Alan", "Bob", "Steve", "Jim", "Aiden", "Rob", "Bill", "Jacob", "James" };

            // Get all strings that start with the letter A
            //List<string> stringsA = new List<string>();
            //foreach (string a in myStrings)
            //{
            //    if (a.StartsWith("A"))
            //        stringsA.Add(a);
            //}

            //foreach (string s in stringsA)
            //    Console.WriteLine(s);

            // Part 2
            Console.WriteLine("Choose - 1 (Begin) or 2 (End):");
            string choice = Console.ReadLine();

            Console.WriteLine("Enter letter:");
            string letter = Console.ReadLine();

            List<string> names = new List<string>();
            if (choice == "1")
            {
                names = GetStringsBeginningWith(myStrings, letter);
            }
            else
            {
                names = GetStringsEndingWith(myStrings, letter);
            }

            foreach (string name in names)
                Console.WriteLine(name);


            Console.ReadLine();
        }




        static List<string> GetStringsBeginningWith(string[] myStrings, string beginningLetter)
        {

            // Get all strings that start with the letter A
            List<string> foundStrings = new List<string>();
            foreach (string a in myStrings)
            {
                if (a.StartsWith(beginningLetter))
                    foundStrings.Add(a);
            }

            return foundStrings;
        }

        static List<string> GetStringsEndingWith(string[] myStrings, string endingLetter)
        {
            List<string> foundStrings = new List<string>();
            foreach (string a in myStrings)
            {
                if (a.EndsWith(endingLetter))
                    foundStrings.Add(a);
            }

            return foundStrings;
        }
    }
}
