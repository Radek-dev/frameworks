﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//enables to pass around a methods into other methods
//delegate part of a method to another method
//for sorting algorithim

namespace DelegatesPart2
{
    class Program
    {
        // Step 1: Define a function type ... just like you would
        // define an object type, except it's a method signature
        // for a function ... in this case, a function that 
        // works with a string.
        //the below line means:
        //public delegate return_type NameOftheDelegate(input parameter type)
        public delegate bool FunctionForString(string s);


        // Note: Why make this so specific to just strings?
        // We have generics now, we could do something like
        // this instead which would allow us to use this
        // function type for any input parameter type, as long 
        // as we only needed it to return a bool and accept 
        // one parameter.
        // public delegate bool FunctionForAnything<T>(T item);
        // We'll do that later.  :)

        // Step 2: Define a method that uses our new delegate
        // as an input parameter.  This particular method will
        // accept a string array, and accept an instance of the
        // delegate (some algorithm to perform on a string), then 
        // for each string in the array, it will apply the algorithm
        // to it the return the array of strings back.
        // For you GoF fans, this is the essence of the "Visitor Pattern".
        public static string[] PerformOperationOnStringArray(string[] myStrings, FunctionForString myFunction)
        {
            System.Collections.ArrayList myList = new System.Collections.ArrayList();
            foreach (string s in myStrings)
            {
                if (myFunction(s))
                {
                    myList.Add(s);
                }
            }

            return (string[])myList.ToArray(typeof(string));

        }

        // Step 3: We'll create a method that -- by it's method
        // signature -- matches the delegate declaration.  There's
        // nothing really attaching the two together ... just the
        // method signature convention.  However, when this method
        // is passed as an argument to the PerformOperationOnStringArray
        // method, it becomes an instance of the delegate.
        public static bool StartsWithA(string s)
        {
            return s.StartsWith("A");
        }

        public static bool EndsWithN(string s)
        {
            return s.EndsWith("n");
        }


        // Step 4: Now, we'll use our PerformOperationOnStringArray, which
        // knows how to loop through an array of strings (passed in as the 
        // the first parameter) and apply an algorithm to it (the method
        // that encapsulates the algorithm is passed in as the second
        // parameter.
        static void Main(string[] args)
        {
            string[] myStrings = { "Adam", "Alan", "Bob", "Steve", "Jim", "Aiden" };

            string[] stringsA = PerformOperationOnStringArray(myStrings, StartsWithA);

            string[] stringsN = PerformOperationOnStringArray(myStrings, EndsWithN);

            foreach (string s in stringsN)
                Console.WriteLine(s);


            Console.ReadLine();
        }
    }
}
