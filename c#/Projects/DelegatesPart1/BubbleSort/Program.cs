﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Sorts LISTS
//delegate is a pointer to a functions, delegates some resposobility
//to another code

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Order> myOrders = setupOrders();

            BubbleSort.Sort<Order>(myOrders, Order.SortByOrderAmount);

            Console.WriteLine("Sort By Order Amount");
            Console.WriteLine("--------------------");
            foreach (Order o in myOrders)
                Console.WriteLine("{0} - {1} - {2}", o.OrderID.ToString(), o.OrderDate.ToShortDateString(), o.OrderAmount.ToString());

            Console.WriteLine("");
            Console.WriteLine("");

            BubbleSort.Sort<Order>(myOrders, Order.SortByOrderDate);

            Console.WriteLine("Sort By Order Date");
            Console.WriteLine("--------------------");
            foreach (Order o in myOrders)
                Console.WriteLine("{0} - {1} - {2}", o.OrderID.ToString(), o.OrderDate.ToShortDateString(), o.OrderAmount.ToString());


            Console.WriteLine("");
            Console.WriteLine("");

            List<int> myIntegers = new List<int>() { 42, 4, 8, 15, 23, 16 };

            BubbleSort.Sort<int>(myIntegers, Utility.SortInteger);

            foreach (int i in myIntegers)
                Console.WriteLine(i.ToString());

            Console.ReadLine();
        }

        static private List<Order> setupOrders()
        {
            List<Order> orderList = new List<Order>();

            Order o1 = new Order();
            o1.OrderDate = DateTime.Parse("12/7/2007 1:05 PM");
            o1.OrderAmount = 500;
            o1.OrderID = 9009;

            OrderItem oi1 = new OrderItem();
            oi1.OrderItemID = 123;
            oi1.ProductName = "FooBar";
            oi1.Quantity = 2;
            o1.OrderItems.Add(oi1);

            OrderItem oi2 = new OrderItem();
            oi2.OrderItemID = 124;
            oi2.ProductName = "BazQuirk";
            oi2.Quantity = 3;
            o1.OrderItems.Add(oi2);

            Order o2 = new Order();
            o2.OrderDate = DateTime.Parse("12/8/2007 9:15 AM");
            o2.OrderAmount = 450;
            o2.OrderID = 9010;

            OrderItem oi3 = new OrderItem();
            oi3.OrderItemID = 125;
            oi3.ProductName = "FooBar";
            oi3.Quantity = 1;
            o2.OrderItems.Add(oi3);

            OrderItem oi4 = new OrderItem();
            oi4.OrderItemID = 126;
            oi4.ProductName = "Gadgets";
            oi4.Quantity = 5;
            o2.OrderItems.Add(oi4);

            OrderItem oi5 = new OrderItem();
            oi5.OrderItemID = 127;
            oi5.ProductName = "Bazquirk";
            oi5.Quantity = 1;
            o2.OrderItems.Add(oi5);

            OrderItem oi6 = new OrderItem();
            oi6.OrderItemID = 128;
            oi6.ProductName = "Widgets";
            oi6.Quantity = 6;
            o2.OrderItems.Add(oi6);

            Order o3 = new Order();
            o3.OrderDate = DateTime.Parse("12/9/2007 4:50 PM");
            o3.OrderAmount = 550;
            o3.OrderID = 9011;

            OrderItem oi7 = new OrderItem();
            oi7.OrderItemID = 129;
            oi7.ProductName = "Widgets";
            oi7.Quantity = 3;
            o3.OrderItems.Add(oi7);

            OrderItem oi8 = new OrderItem();
            oi8.OrderItemID = 130;
            oi8.ProductName = "Gadgets";
            oi8.Quantity = 5;
            o3.OrderItems.Add(oi8);

            Order o4 = new Order();
            o4.OrderDate = DateTime.Parse("12/10/2007 1:23 PM");
            o4.OrderAmount = 350;
            o4.OrderID = 9012;

            OrderItem oi9 = new OrderItem();
            oi9.OrderItemID = 131;
            oi9.ProductName = "Widgets";
            oi9.Quantity = 1;
            o4.OrderItems.Add(oi9);

            orderList.Add(o1);
            orderList.Add(o2);
            orderList.Add(o3);
            orderList.Add(o4);

            return orderList;
        }
    }

    class Order
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public decimal OrderAmount { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public Address BillingAddress { get; set; }
        public Address ShippingAddress { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public Order(int orderID, DateTime orderDate)
        {
            OrderID = orderID;
            OrderDate = orderDate;
        }

        //======================================================
        // Here are the two SortComparer delegate instances
        //======================================================

        static public bool SortByOrderAmount(Order o1, Order o2)
        {
            return o1.OrderAmount < o2.OrderAmount;
        }

        static public bool SortByOrderDate(Order o1, Order o2)
        {
            return o1.OrderDate < o2.OrderDate;
        }
    }

    class OrderItem
    {
        public int OrderItemID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }

    class Address
    {
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }


    // =========================================================
    // I added this to create the SortInteger ... please note, a sorting
    // feature of Integers already exists in the .NET Framework Class
    // library.  All I'm doing here is showing how our BubbleSort.Sort
    // method can be used for any type when you send in the SortComparer
    // delegate.
    // =========================================================
    class Utility
    {
        static public bool SortInteger(int i1, int i2)
        {
            return i1 < i2;
        }
    }

    // =========================================================
    // Here's my delegate definition
    // =========================================================

    delegate bool SortComparer<T>(T firstItem, T secondItem);


    // =========================================================
    // And this is where all the magic happens ... we accept BOTH
    // your data and a method that compares two data bits to
    // determine which one is sorted before the other.  The same
    // basic algorithm works no matter what the type is from that
    // point on!
    // ==========================================================
    class BubbleSort
    {
        static public void Sort<T>(IList<T> sortList, SortComparer<T> comparison)
        {
            bool sorted = true;
            do
            {
                sorted = false;
                for (int i = 0; i < sortList.Count - 1; i++)
                {
                    if (comparison(sortList[i + 1], sortList[i]))
                    {
                        T temp = sortList[i];
                        sortList[i] = sortList[i + 1];
                        sortList[i + 1] = temp;
                        sorted = true;
                    }
                }
            } while (sorted);

        }
    }

}
