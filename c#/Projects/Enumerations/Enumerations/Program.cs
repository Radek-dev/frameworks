﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//ENUMERATIONS  are predefined variable or properties that we can call or use
//each object will have its enumarations we can call like color of a foreground
//To constrain the number of values a given property or variable can be set to
//Enumeration = data type that have defined certain type of values - can't set it to invalied value

namespace Enumerations
{
    class Program
    {
        static void Main(string[] args)
        {
            //so you can't change the color of the string here
            Console.ForegroundColor = ConsoleColor.DarkRed;

            Console.WriteLine("Hello");

            Console.WriteLine("Write a her:");


            string uservalue = Console.ReadLine();

            SuperHero myValue;

            //try to parse a string into one of the emun
            // (string to parse, ingnor capitalisation, ouput parameter but as string - new variable
            if (Enum.TryParse<SuperHero>(uservalue, true, out myValue))
            {

                switch (myValue)
                {
                    case SuperHero.Batman:
                        Console.WriteLine("Caped crusader");
                        break;
                    case SuperHero.Superman:
                        break;
                    case SuperHero.GreenLantern:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Console.WriteLine("Does not compute");
            }

            Console.WriteLine(myValue);
            Console.ReadLine();
        }
    }

    // create a enumeration
    // strongly typed - can't change it - less likely to break
    // weakly typed - may require checking when running the code
    enum SuperHero
    {
        Batman,
        Superman,
        GreenLantern
    }
}
