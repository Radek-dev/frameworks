﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;


namespace ADONET2ConnectedExpress
{
    class Program
    {
        static void Main(string[] args)
        {
            // Step 1 Handshake with the database
            //'.' means my local SQL Express connection
            //

            SqlConnection myConnection = new SqlConnection("Data Source=.\\SQLEXPRESS;" +
                            "AttachDbFilename=|DataDirectory|\\Database1.mdf; Integrated Security = True; User Instance=True");

            myConnection.Open();

            // Step 2 Specify the command (request)

            SqlCommand MyCommand = myConnection.CreateCommand();

            //this is the string to send to the database
           // MyCommand.CommandText = "SELECT * FROM Customers";
           // MyCommand.CommandType = System.Data.CommandType.Text;

            //or use the stored procedure
            MyCommand.CommandText = "GetCustomers";
            MyCommand.CommandType = System.Data.CommandType.StoredProcedure;

            
            //both ways return the same result

            SqlDataReader myDataReader;

            myDataReader = MyCommand.ExecuteReader();

            // Step 3 Use the data

            while (myDataReader.Read())
            {
                Console.WriteLine(myDataReader[1] + " " + myDataReader[2]);

            }

            myConnection.Close();

            //holding the connection for long here
            Console.ReadLine();
        }
    }
}
