﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperMethods
{
    class Program
    {
        //if 'void', then it does not returns anything
        static void Main(string[] args)
        {
            //cass the method
            //naming convetion for the method
            //method actually called depends on which variables are called
            //variables to pass are via the methed are indentifying the method as well as name - data type is the method's signiture
            string myValue = superSecretFormula("Bob");
            Console.WriteLine(myValue);
            Console.ReadLine();

        }

        //create a new method here - methods looks like a function
        // here the string refers to the return value
       
        private static string superSecretFormula()
        {
            // some cool stuff here
            return "Hello World";
        }

        private static string superSecretFormula(string name)
        {
            // some cool stuff here
            return String.Format("Hello, {0} ", name);
        }

    }
}
