﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            //escape sequences

            string myString = "Goto your c:\\ drive";
            myString = "My \" So-called called \" life";
            myString = "How do I create a new \n line?";

            myString = String.Format("Make: {0} (Model: {1})", "BMW", "745Li");

            

            Console.WriteLine(myString);
            Console.ReadLine();
        }

    }
}
