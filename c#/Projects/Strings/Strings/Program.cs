﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            // '\' has a special meaning, it is an escape character
            // '\\' means \
            // full list: http://blogs.msdn.com/b/csharpfaq/archive/2004/03/12/what-character-escape-sequences-are-available.aspx

            string myString = "Go to your c:\\ drive"
                            + " My \"so called\" life"
                            + "What if I need \n a new line? \a" ;

            Console.WriteLine(myString);


            Console.ReadLine();

            //beep
            myString = "\a";
            
            Console.WriteLine(myString);


            Console.ReadLine();

            //simple substitution
            myString = string.Format ("{0}!", "Bonzai");

            Console.WriteLine(myString);


            Console.ReadLine();


            //reple two strings
            myString = string.Format("{0}! and {1}", "Bonzai", "HI");

            Console.WriteLine(myString);


            Console.ReadLine();


            //formating - for currency - displays £ - N for number, C for currency
            myString = string.Format("{0:C}", 123.45);

            Console.WriteLine(myString);


            Console.ReadLine();

            //formating phone number - or any string
            myString = string.Format("Phone number: {0:(###) ###-###}", 123123123);

            Console.WriteLine(myString);


            Console.ReadLine();

            //details: http://msdn.microsoft.com/en-us/library/system.string.format(v=vs.110).aspx

            //When concatenating strings over a loop use the below - to save memory - use StringBuilder

            StringBuilder myStr = new StringBuilder();

            for (int i = 0; i < 100; i++)
            {
                myStr.Append("--");
                myStr.Append(i);
            }

            
            //this converts to string
            myString = myStr.ToString();

            //cut out strings
            //editing a string: 
            myString = myString.Substring(2, 4);


            Console.WriteLine(myString);


            Console.ReadLine();

            // string methods: http://msdn.microsoft.com/en-us/library/hh423731.aspx

        }
    }
}
