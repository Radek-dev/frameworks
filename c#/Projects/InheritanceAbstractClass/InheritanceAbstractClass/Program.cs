﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceAbstractClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myCar = new Car();
            myCar.Make = "BMW";
            myCar.Model = "745li";
            myCar.Color = "Black";
            //myCar.TowindCapacity = 1221; - notworking

            printVehicleDetails(myCar);

            Truck myTruck = new Truck();
            myTruck.Make = "Ford";
            myTruck.Model = "F65";
            myTruck.Color = "White";

            //this is the Truck class specific property
            myTruck.TowingCapacity = 1200;

            printVehicleDetails(myTruck);

            Console.ReadLine();

        }

        private static void printVehicleDetails(Vehicle vehicle)
        {
            Console.WriteLine("Here are the Vehicle's details: {0}",
                vehicle.FormatMe());
        }
    }
    //ABSTRACT CLASS
    //can't work without specific implementation
    abstract class Vehicle
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }

        //this is saying that you have to have the implicatation
        public abstract string FormatMe();

    }
    class Car : Vehicle
    {
        //'virtual' means: if you want to overwrite me, you can, if not, it is fine
        //'abstract' means: if you want to use this, you must overwrite me, it does not support anything, must be overwritten
        public override string FormatMe()
        {
            return String.Format("{0} - {1} - {2} - {3}",
                    this.Make,
                    this.Model,
                    this.Year,
                    this.Color);
        }
    }

    //'sealed' means that you can't inherite from the class - no subclasses for Truck
    sealed class Truck : Vehicle
    {
        public int TowingCapacity { get; set; }

        //can overwrite a method within a class the default
        //can 
        public override string FormatMe()
        {
            return String.Format("{0} - {1} - {2} Towing Units ",
                    this.Make,
                    this.Model,
                    this.TowingCapacity);
        }

    }
    //inharitance saves typing code
    //anywhere where Car would work Truck would work as well.
    //Truck is type of Car
    //Car is base class (parent, superclass), Truck is derivared class (child, subclass)
    //Truck is specific
    //help on classes: http://msdn.microsoft.com/en-us/library/system.io.filestream(v=vs.110).aspx
}
