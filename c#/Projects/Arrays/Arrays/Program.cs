﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //declare an array
            int[] numbers = new int[5];


            //REMEMBER that the first element has 0 address
            numbers[0] = 4;
            numbers[1] = 8;
            numbers[2] = 15;
            numbers[3] = 16;
            numbers[4] = 23;

            //can't declar
           // numbers[6] = 45;

            //can't expand an array once declared
            

            //different declaration for the array

            int[] numbers2 = new int[] { 4, 8, 15, 16, 23, 45 };

            Console.WriteLine(numbers2[1].ToString());
            Console.ReadLine();


            //string array

            string[] names = new string[] { "Eddie", "Alex", "Mike" };

            //can use foreach with arrays
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            Console.ReadLine();

            // this is how to concatenate strings
            string zig = "Hi" +
                            " Hello";

            Console.WriteLine(zig);
            Console.ReadLine();
            
            //data type 'char' is for single letters only - only single alphanumerate character
            //ToCharArray splits a string into char type
            char[] charArray = zig.ToCharArray();
            //reverse takes each element in the array and it reverses its position
            Array.Reverse(charArray);

            foreach (char zigChar in charArray)
                Console.Write(zigChar);

            //returns the number of elements in the array
            Console.WriteLine(numbers2.Length);
            
            Console.ReadLine();



        }
    }
}
