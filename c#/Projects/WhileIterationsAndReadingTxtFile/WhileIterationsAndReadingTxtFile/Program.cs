﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileIterationsAndReadingTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {

            int a = 1, b = 2;

            //StreamReader is a object - must declare
            //must be from correct name space
            //remember you can resolve the issue with referencing the using space via this IDE
            StreamReader myReader = new StreamReader("Values1.txt");
            string line = "";

            //if you do not know how many iterations you need to run
            while (line != null)
            {
                line = myReader.ReadLine();
                if (line != null)
                    Console.WriteLine(line);
            }

            //must remember to close the open file - stays open
            myReader.Close();
            Console.ReadLine();

        }
    }
}
