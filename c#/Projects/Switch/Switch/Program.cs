﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Switch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type in a super hero's name to see his nichname:");
            string userValue = Console.ReadLine();
            
            //break is done to stop the switch
            //the same as many if statements
            //if userValue is typed in different capitalisations
            //use replace to get rid of all spaces
            switch (userValue.ToUpper().Replace(" ",""))
	            {
                case "BATMAN":
                        Console.WriteLine("Capped Crusader");
                        break;
                case "SUPERMAN":
                        Console.WriteLine("Man of Steel");
                        break;
		        default:
                        Console.WriteLine("I do not know");
                        break;
	            }

        }
    }
}
