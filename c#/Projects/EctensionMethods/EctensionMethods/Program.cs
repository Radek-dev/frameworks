﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//add functionality to a class over these methods/properties

namespace EctensionMethods
{
    //REMEBER that if we want to use extention methods, it must be used in a static class
    static class Program
    {
        static void Main(string[] args)
        {

            int i = 7;

            //this is one way
            Print(i);

            //this is the extension
            i.StaticPrint();

            //the extension
            int anotherNumber = 10;
            anotherNumber.Increment().Bonus().Double().Decrement().StaticPrint();

            Console.ReadLine();
        }

        static void Print(object o)
        {
            Console.WriteLine("Calleed pring: {0}", o.ToString());
        }

        //must be 'static' and must have 'this' - both required for extensions 
        static void StaticPrint(this object o)
        {
            Console.WriteLine("Calleed pring: {0}", o.ToString());
        }


        // for i++ meaning https://uk.answers.yahoo.com/question/index?qid=20110511063045AArmWRN
        public static int Increment(this int i)
        {
            return i++;
        }

        public static int Decrement(this int i)
        {
            return i--;
        }

        public static int Double(this int i)
        {
            return i * 2;
        }

        public static int Bonus(this int i)
        {
            return i + 5;
        }
    }
}
