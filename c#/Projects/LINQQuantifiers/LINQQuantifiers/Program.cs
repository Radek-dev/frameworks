﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//LISTs all the extension methods available in LINQ 

namespace LINQQuantifiers
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers1 = { 46, 24, 79, 35, 12, 57, 80, 68 };

            //true if there are any number
            //var expr = numbers1.Any();

            //Do all the number are greater than 30?
            //var expr = numbers1.All(o => o > 30);

            //false if there is not 30
            //var expr = numbers1.Contains(30);

            //how many items?
            // var expr = numbers1.Count();

            //what is the sum?
            //var expr = numbers1.Sum();

            //what is the min/smallest number?
            //var expr = numbers1.Min();

            //largest number?
            // var expr = numbers1.Max();

            //what is the average?
            //var expr = numbers1.Average();

            //what is the first item?
            //var expr = numbers1.First();

            //what si the last item?
            //var expr = numbers1.Last();

            //return the first item in the sequence that satisfies this condition or return the defaluat - for integer it is 0 as integer
            //var expr = numbers1.FirstOrDefault(o => o > 100);

            //last item of the sequence that satisfies this condition or give me the default - zero for integer
            // var expr = numbers1.LastOrDefault(o => o > 60);

            //one item that satisfies the condition or return an error - exeptions if more than one item
            //var expr = numbers1.Single(o => o > 49);

            //this handles the zero
            //var expr = numbers1.SingleOrDefault(o => o > 49);

            //perform sum together - sum, seed is 10 
            //var expr = numbers1.Aggregate(10, (o, p) => o > p ? o : p);

            //performs sum of all the values
            var expr = numbers1.Aggregate(10, (o, p) => o += p);

            Console.WriteLine(expr);
            Console.ReadLine();
        }
    }
}
