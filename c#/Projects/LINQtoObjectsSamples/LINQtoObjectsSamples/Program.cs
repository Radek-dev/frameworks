﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQtoObjectsSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] countries = new string[] {"USA", 
                "Canada", "UK", "Australia", "Germany", "Sweeden", 
                "Mexico", "France", "Italy", "Uganda", "Ukraine", "Spain"};

            // Here we start with a pretty basic way of looping through each item
            // in an array and printing it if it matches the criteria
            //foreach (string country in countries)
            //{
            //    if (country.StartsWith("U"))
            //        Console.WriteLine(country);
            //}


            // We could do this instead
            //var uCountries = from c in countries
            //                 where c.StartsWith("U")
            //                 select c;

            //foreach (string country in uCountries)
            //    Console.WriteLine(country);


            // Or even this!
            //var uCountries = countries.Where(p => p.StartsWith("U"));

            //foreach (string country in uCountries)
            //    Console.WriteLine(country);


            // Or even this!
            //foreach (string country in countries.Where(p => p.StartsWith("U"))) 
            //    Console.WriteLine(country);


            // Ok, how about using a more complicated lambda expression
            //foreach (string country in countries.Where(p => p.Length > 5))
            //    Console.WriteLine(country);


            // Here's another complicated lambda … can you tell what it's doing?
            //foreach (string country in countries.OrderBy(p => p))
            //    Console.WriteLine(country);

            // Here's the opposite … showing just how many extension methods there are
            // to work with
            //foreach (string country in countries.OrderByDescending(p => p))
            //    Console.WriteLine(country);

            // There's also methods for traversal through the collection / array
            //foreach (string country in countries.Skip(3))
            //    Console.WriteLine(country);

            //foreach (string country in countries.Take(3))
            //    Console.WriteLine(country);


            // Console.WriteLine(countries.First());

            //var myCountries = from country in countries
            //                  where country.Length > 5
            //                  select country;


            //var myCountries = from country in countries
            //                  where country.StartsWith("U")
            //                  && country.Length > 2
            //                  select country;


            //var myCountries = countries.Where(p => p.StartsWith("U") && p.Length > 5);


            //var myCountries = from country in countries
            //                  orderby country
            //                  select country;


            //var myCountries = from country in countries
            //                  orderby country descending
            //                  select country;

            //var myCountries = from country in countries.Skip(3)
            //                  select country;

            var myCountries = from country in countries.Take(3)
                              select country;

            foreach (string country in myCountries)
                Console.WriteLine(country);



            Console.WriteLine("");
            Console.WriteLine("Press enter to continue ...");
            Console.ReadLine();
        }
    }
}
