﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfStatements
{
    class Program
    {
        static void Main(string[] args)
        {
            ////this is just to show the code
            //Console.WriteLine("Please type somehting and press the Enter key.");
            //string userValue;
            //userValue = Console.ReadLine();
            //Console.WriteLine("You typed: " + userValue);

            Console.WriteLine("Write 1, 2, or 3");
            string userValue = Console.ReadLine();
            string message;
            
            ////if statement
            //if (userValue == "1")
            //{
            //    Console.WriteLine("You won a car");
            //}
            //else if (userValue=="2")
            //{
            //    Console.WriteLine("You won a boat");
            //}
            //else if (userValue=="3")
            //{
            //    Console.WriteLine("You wan a cat");
            //}
            //else
            //{
            //    Console.WriteLine("Sorry, wrong number.");
            //}

            //can rewrtie:

            if (userValue == "1")
            {
                message="You won a car";
            }
            else if (userValue == "2")
            {
                message="You won a boat";
            }
            else if (userValue == "3")
            {
                message="You wan a cat";
            }
            else
                message="Sorry, wrong number.";     //can omit the curly brakets

            Console.WriteLine(message);

            Console.ReadLine();

            
            string message2= (userValue == "1") ? "boat - string if true" : "string if not true";
            //this is a string replacement syntax, zero is replaced by message 2
            Console.WriteLine("You won {0} and valued entered is: {1}", message2, userValue);
            //this is just one replacement syntax for message2
            Console.WriteLine("You won {0}", message2);

            Console.ReadLine();

        }
    }
}
