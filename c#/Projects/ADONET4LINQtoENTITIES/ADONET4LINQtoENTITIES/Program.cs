﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADONET4LINQtoENTITIES
{
    class Program
    {
        static void Main(string[] args)
        {
        
            Database1Entities1 de = new Database1Entities1();

            //pulls back entity set of customers - changed into T-SQL to return the relevant data
            //filter as much as you can to return only relevant data column - only the rows that are needed
            //query is not executed until we needed in the code
            var customers = from c in de.Customers
                            where c.Orders.Any(o => o.OrderAmount > 150)
                            select c;

            foreach (var c in customers)
            {
                Console.WriteLine(c.LastName + " Orders: " + c.Orders.Count().ToString());

                //these will consume a lot of memory
                foreach (var o in c.Orders)
                {
                    Console.WriteLine(o.OrderDate.ToShortDateString() + " - " + o.OrderAmount.ToString());
                }
                
            }



            //Console.WriteLine(customers.First().FirstName + " Orders:" + customers.Orders.Count());

            Console.ReadLine();


        }
    }
}
