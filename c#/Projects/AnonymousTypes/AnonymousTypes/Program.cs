﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//AnomymousTypes

namespace AnonymousTypes
{
    class Program
    {
        static void Main(string[] args)
        {
           // // In the previous video we did this:
           // //new order - use the initiliser
           // //data type of the order1 is Anonymous as it is an instance of a class
           // Order order1 = new Order { OrderID = 123, OrderAmount = 395.00M, OrderDate = DateTime.Parse("12/7/2008 8:08 AM") };

           // // It's pretty obvious what its type is, right?
           // Console.WriteLine("The datatype for order1 is {0}", order1.GetType());


           // //this one is really order as data type
           // var order2 = new Order { OrderID = 124, OrderAmount = 425.00M };
           // Console.WriteLine("The datatype for order2 is {0}", order2.GetType());


           // var order3 = new { OrderID = 125, OrderAmount = 375.00M };
           // Console.WriteLine("The datatype for order3 is {0}", order3.GetType());
           // //with new it assigns some machine datatype


           // var order4 = new { OrderID = 126, OrderAmount = 440.00M };
           // Console.WriteLine("The datatype for order4 is {0}", order4.GetType());
           // //the same type as order3

           // var order5 = new { OrderID = 127, OrderAmount = 125.00M, MyNewProperty = "ABC" };
           // Console.WriteLine("The datatype for order5 is {0}", order5.GetType());
           //// here is new property - this creates new type of the order5
            

           // var order6 = new { OrderAmount = 250.00M, OrderID = 128 };
           // Console.WriteLine("The datatype for order6 is {0}", order6.GetType());
           // //switched around order amount 250.00M and OrderID - diferent type


           // var order7 = new { MyOrderID = 129, MyOrderAmount = 175.00M };
           // Console.WriteLine("The datatype for order7 is {0}", order7.GetType());

            //calling method setupOrder and returning a list of orders
            List<Order> orderList = setupOrders();


            var myOrders = from o in orderList
                           //create new anonymous type that can the below properties
                           select new
                           {
                               //pulling the order ID
                               OrderID = o.OrderID,
                               //pulling up all the quantities and sum them
                               ItemsToShip = o.OrderItems.Sum(p => p.Quantity),
                               DateOfOrder = o.OrderDate
                           };


            Console.WriteLine("The datatype for myOrders is {0}", myOrders.GetType());


            foreach (var whoKnows in myOrders)
                Console.WriteLine("     OrderID: {0}, ItemsToShip: {1}", whoKnows.OrderID.ToString(), whoKnows.ItemsToShip.ToString());

            Console.ReadLine();

        }


        static private List<Order> setupOrders()
        {
            List<Order> orderList = new List<Order>();

            Order o1 = new Order();
            o1.OrderDate = DateTime.Parse("12/7/2007 1:05 PM");
            o1.OrderID = 9009;

            OrderItem oi1 = new OrderItem();
            oi1.OrderItemID = 123;
            oi1.ProductName = "FooBar";
            oi1.Quantity = 2;
            o1.OrderItems.Add(oi1);

            OrderItem oi2 = new OrderItem();
            oi2.OrderItemID = 124;
            oi2.ProductName = "BazQuirk";
            oi2.Quantity = 3;
            o1.OrderItems.Add(oi2);

            Order o2 = new Order();
            o2.OrderDate = DateTime.Parse("12/8/2007 9:15 AM");
            o2.OrderID = 9010;

            OrderItem oi3 = new OrderItem();
            oi3.OrderItemID = 125;
            oi3.ProductName = "FooBar";
            oi3.Quantity = 1;
            o2.OrderItems.Add(oi3);

            OrderItem oi4 = new OrderItem();
            oi4.OrderItemID = 126;
            oi4.ProductName = "Gadgets";
            oi4.Quantity = 5;
            o2.OrderItems.Add(oi4);

            OrderItem oi5 = new OrderItem();
            oi5.OrderItemID = 127;
            oi5.ProductName = "Bazquirk";
            oi5.Quantity = 1;
            o2.OrderItems.Add(oi5);

            OrderItem oi6 = new OrderItem();
            oi6.OrderItemID = 128;
            oi6.ProductName = "Widgets";
            oi6.Quantity = 6;
            o2.OrderItems.Add(oi6);

            Order o3 = new Order();
            o3.OrderDate = DateTime.Parse("12/9/2007 4:50 PM");
            o3.OrderID = 9011;

            OrderItem oi7 = new OrderItem();
            oi7.OrderItemID = 129;
            oi7.ProductName = "Widgets";
            oi7.Quantity = 3;
            o3.OrderItems.Add(oi7);

            OrderItem oi8 = new OrderItem();
            oi8.OrderItemID = 130;
            oi8.ProductName = "Gadgets";
            oi8.Quantity = 5;
            o3.OrderItems.Add(oi8);

            Order o4 = new Order();
            o4.OrderDate = DateTime.Parse("12/10/2007 1:23 PM");
            o4.OrderID = 9012;

            OrderItem oi9 = new OrderItem();
            oi9.OrderItemID = 131;
            oi9.ProductName = "Widgets";
            oi9.Quantity = 1;
            o4.OrderItems.Add(oi9);

            orderList.Add(o1);
            orderList.Add(o2);
            orderList.Add(o3);
            orderList.Add(o4);

            return orderList;
        }
    }

    class Order
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public decimal OrderAmount { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public Address BillingAddress { get; set; }
        public Address ShippingAddress { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public Order(int orderID, DateTime orderDate)
        {
            OrderID = orderID;
            OrderDate = orderDate;
        }
    }

    class OrderItem
    {
        public int OrderItemID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }

    class Address
    {
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
