﻿using System;

//asd
namespace After037_NewVirtualOverride
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var baseClass = new BaseClass();
            var derivedOverride = new DerivedOverride();
            var derivedNew = new DerivedNew();
            var derivedOverWrite = new DerivedOverwrite();

            baseClass.Name();
            derivedOverride.Name();
            derivedNew.Name();
            derivedOverWrite.Name();

            Console.ReadLine();
            baseClass.Name();
            derivedOverride.Name();
            //this is the casting - implementing what is done in the base
            ((BaseClass) derivedNew).Name();
            ((BaseClass) derivedOverWrite).Name();
            Console.ReadLine();

            var t1 = typeof (BaseClass);
            Console.WriteLine(t1.Name);
            Console.WriteLine(t1.Assembly);
        }
    }

    //highest class
    internal class BaseClass
    {
        //virtual means that this can be overwritten by inheriting class
        internal virtual void Name()
        {
            Console.WriteLine("BaseClass");
        }
    }

    internal class DerivedOverride : BaseClass
    {
        //this is the override of the method
        internal override void Name()
        {
            Console.WriteLine("DerivedOverride");
        }
    }

    internal class DerivedNew : BaseClass
    {
        //'new' means means that it has no relationship to the original one - it is replaced
        //unique to the DerivedNew
        internal new void Name()
        {
            Console.WriteLine("New");
        }
    }

    internal class DerivedOverwrite : BaseClass
    {
        //hide the base implementation - likely to cause problem
        internal void Name()
        {
            Console.WriteLine("Overwrite");
        }
    }
}