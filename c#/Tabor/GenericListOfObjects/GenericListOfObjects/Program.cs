﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericListOfObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            // list is of type generic collection 
			// generic means every item in the collection is going to be the same datatype
            // collection is generic for the specific data type
            // new generic list is below
            // list of automobiles (list of t - means of any type)
            // <> for passing specific data type - generic is turned to specific
            List<Automobile> inventory = new List<Automobile>();
            
            Automobile a1 = new Automobile();
            a1.Make = "Dodge";
            a1.Model = "Dart";
            a1.Year = 1976;
            a1.ExteriorColor = "Green";
            a1.Miles = 111001;

            Automobile a2 = new Automobile();
            a2.Make = "Oldsmobile";
            a2.Model = "Cutlas Supreme";
            a2.Year = 1985;
            a2.ExteriorColor = "Silver";
            a2.Miles = 75001;

            Automobile a3 = new Automobile();
            a3.Make = "Geo";
            a3.Model = "Prism";
            a3.Year = 1992;
            a3.ExteriorColor = "Green";
            a3.Miles = 154001;

            Automobile a4 = new Automobile();
            a4.Make = "Nissan";
            a4.Model = "Altima";
            a4.Year = 2000;
            a4.ExteriorColor = "Black";
            a4.Miles = 105001;

            Automobile a5 = new Automobile();
            a5.Make = "BMW";
            a5.Model = "745Li";
            a5.Year = 2005;
            a5.ExteriorColor = "Black";
            a5.Miles = 70001;

            inventory.Add(a1);
            inventory.Add(a2);
            inventory.Add(a3);
            inventory.Add(a4);
            inventory.Add(a5);
            // try typing 
            //inventory.Add(
            // so it knows that the list is not generic now but type of automobile
  
            
            Console.WriteLine("We have the following items in inventory: ");
            // does not require any casting
            // it knows the current data type
            foreach (Automobile item in inventory)
            {
                // now casting but reference to Automobile right away
                item.Print();
            }

            Console.WriteLine("");						// can reference year here as the list of known
            Console.WriteLine("Our oldest car was built in {0}.", inventory[0].Year );

            Console.ReadLine();

            // Ok, this still won't work for the Book example, though ...
            // BUT at least this time, we get an error message AT DESIGN TIME!

            Book b1 = new Book();
			
			// this is the problem
			// invetory.Add(b1);
			
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";

            // the problem is that we can't any other data type
            inventory.Add(b1);

            // But how to fix?

        }
    }
}
