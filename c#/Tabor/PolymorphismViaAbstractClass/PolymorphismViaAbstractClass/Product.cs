﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaAbstractClass
{
    abstract class Product
    {
        public int ProductID { get; set; }

        public abstract void Print();

    }
}
