﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaAbstractClass
{
    class Automobile : Product 
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public int Miles { get; set; }
        public string ExteriorColor { get; set; }

        public override void Print()
        {
            Console.WriteLine("{0} ... {1} {2} {3} with {4} exterior (Odometer: {5})", base.ProductID, Year, Make, Model, ExteriorColor, Miles);
        }
    }
}
