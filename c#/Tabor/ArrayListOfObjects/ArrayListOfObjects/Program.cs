﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// IMPORTANT:  Add this:
// Arraylist is a type of a collection from Collectins
using System.Collections;

namespace ArrayListOfObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            // Part 1

            // invesntory is of type ArrayList - can take the below actions
            ArrayList inventory = new ArrayList();

            Automobile a1 = new Automobile();
            a1.Make = "Dodge";
            a1.Model = "Dart";
            a1.Year = 1976;
            a1.ExteriorColor = "Green";
            a1.Miles = 111001;

            Automobile a2 = new Automobile();
            a2.Make = "Oldsmobile";
            a2.Model = "Cutlas Supreme";
            a2.Year = 1985;
            a2.ExteriorColor = "Silver";
            a2.Miles = 75001;

            Automobile a3 = new Automobile();
            a3.Make = "Geo";
            a3.Model = "Prism";
            a3.Year = 1992;
            a3.ExteriorColor = "Green";
            a3.Miles = 154001;

            Automobile a4 = new Automobile();
            a4.Make = "Nissan";
            a4.Model = "Altima";
            a4.Year = 2000;
            a4.ExteriorColor = "Black";
            a4.Miles = 105001;

            Automobile a5 = new Automobile();
            a5.Make = "BMW";
            a5.Model = "745Li";
            a5.Year = 2005;
            a5.ExteriorColor = "Black";
            a5.Miles = 70001;

            // add operator - add each of the instances to the list
            inventory.Add(a1);
            inventory.Add(a2);
            inventory.Add(a3);
            inventory.Add(a4);
            inventory.Add(a5);

            Console.WriteLine("We currently have the following in our inventory:");

            // for each object - go over the whole list
            // object is the father of all objects - it can hold anything
            foreach (object item in inventory)
            {
                // cast the item to an automobile class
                // casting for changing the user defined datatypes
                // this object is actually an Automobile
                Automobile auto = (Automobile)item;
                // after casting we can access the methods and properties of the class
                auto.Print();
            }

            Console.WriteLine("");
            Console.WriteLine("We currently have a special promotion for these items:");

            Automobile promoAuto = (Automobile)inventory[1];
            promoAuto.Print();

            Console.ReadLine();



            // There are some added features that we get from collections:
            Console.WriteLine("");
            Console.WriteLine("Inserting a new item at position 3 ...");

            // Can add one more item to the list
            Automobile a6 = new Automobile();
            a6.Make = "Chrysler";
            a6.Model = "300M";
            a6.ExteriorColor = "Blue";
            a6.Year = 2002;
            a6.Miles = 154001;
            // inserting the atem to he 3rd position - would not work with an array
            inventory.Insert(3, a6);

            // can remove from the list
            Console.WriteLine("Removing item 2 ...");
            inventory.RemoveAt(2);

            // can remove casts of the items from the list
            // it can idenified the by the item
            Console.WriteLine("Removing item 1 ...");
            Automobile removeAuto = (Automobile)inventory[1];
            inventory.Remove(removeAuto);

            Console.WriteLine("");
            Console.WriteLine("Our ArrayList now has {0} items.", inventory.Count);

            Console.WriteLine("");
            foreach (object item in inventory)
            {
                Automobile auto = (Automobile)item;
                auto.Print();
            }

            Console.ReadLine();
            // try to add a different object ot the ArrayList
            Book b1 = new Book();
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-672-32088-6";

            inventory.Add(b1);

            // Now, watch the fireworks!

            Console.WriteLine("");
            Console.WriteLine("Adding books to our inventory!");
            foreach (object item in inventory)
            {
                // Book is not Automobile
                // This is not a valid cast
                // better way to go around this 
                Automobile auto = (Automobile)item;
                auto.Print();
            }

            // Note ... we could hammer this idea through, but it really 
            // isn't worth the effort ... there's a better way.






        }

    }
}
