﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolymorphismViaInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IPrintable> inventory = new List<IPrintable>();

            Automobile a1 = new Automobile();
            a1.Make = "Dodge";
            a1.Model = "Dart";
            a1.Year = 1976;
            a1.ExteriorColor = "Green";
            a1.Miles = 111001;

            Book b1 = new Book();
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";

            inventory.Add(a1);
            inventory.Add(b1);

            Console.WriteLine("We have the following items in stock:");
            foreach (IPrintable item in inventory)
            {
                item.Print();
            }

            Console.ReadLine();
        }
    }
}
