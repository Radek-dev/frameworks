﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    static class Print
    {
        public static void ToConsole(IPrintable printableObject)
        {
            printableObject.Print();
        }

    }
}
