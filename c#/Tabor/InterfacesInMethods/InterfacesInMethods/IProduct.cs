﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    public interface IProduct
    {
        void GetStorageLocation();
    }
}
