﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            // Part 1

            Automobile a1 = new Automobile();
            a1.Make = "Dodge";
            a1.Model = "Dart";
            a1.Year = 1976;
            a1.ExteriorColor = "Green";
            a1.Miles = 111001;

            Book b1 = new Book();
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";


            Print.ToConsole(a1);
            Print.ToConsole(b1);

            Console.ReadLine();

            // Part 2

            Console.WriteLine("");
            Console.WriteLine("Where is our oldest product located?");

            Warehouse myWarehouse = new Warehouse();
            myWarehouse.Add(a1);
            myWarehouse.Add(b1);

            var myOldestProduct = myWarehouse.GetOldestItem();
            myOldestProduct.GetStorageLocation();

            Console.ReadLine();

            // Part 3 ... does this approach make it easy to work with new classes?  You bet!
            Console.WriteLine("");
            Console.WriteLine("Adding ...");
            Toy newToy = new Toy() { Manufacturer = "Kenner", Name = "Luke Skywalker", Type = "Action Figure" };
            Print.ToConsole(newToy);

            Console.WriteLine("");
            Console.WriteLine("Where is our newest product located?");
            myWarehouse.Add(newToy);
            var myNewestProduct = myWarehouse.GetNewestItem();
            myNewestProduct.GetStorageLocation();
            Console.ReadLine();


        }
    }
}
