﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfacesInMethods
{
    interface IPrintable
    {
        void Print();
    }
}
