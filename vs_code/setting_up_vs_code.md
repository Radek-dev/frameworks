# Tcl Interactive setup for VS Code

1. install VS Code if you don't have it and open the project folder where you keep your `Tcl` files
2. install `Tcl` extention in VS Code, this is for code highlighting and snippets
3. install Shell Launcher extention in VS Code, this is for launching a `Tcl` terminal
4. in VS Code keyboard shortcuts search for:

    * run Selected Text in Active Terminal and set it to the choice of your liking
    * Shell Launcher: Launch and set it to the choice of your liking

5. find `settings.json` file in your project in the folder `.vscode` and the below

    ```json
        {
            "shellLauncher.shells.linux": [
                {
                    "shell": "tclsh",
                    "label": "tcl"
                },
                {
                    "shell": "/bin/bash",
                    "label": "bash"
                }
            ]
        }
    ```

6. start `Tcl` terminal using the short cut from step 4 and execute `Tcl` commands using  the other shortcut.
