# Vim Notes

## Links
https://vi.stackexchange.com/questions/422/displaying-tabs-as-characters

## Some Commands

~ - Switch case
c - Change the text
d - Delete
y - Yank
r - Replace
x - Delete
I - Insert
J - Join
u - Make lowercase
U - Make uppercase
> - Shift right
< - Shift left
p - paragraph

## Thinking in VIM
cut-copy-paste = delete-yank-put

[count]operation[count]{motion}

3w = repeat word motiong 3 times

d3w = delete the 3w motion

2d3w = delete the 3w motion 2 times


## Help system

<ctrl+]> find definition of the item in the help system
<ctrl-0> jump back after the above
<ctrl-ww> rotate the cursor over split screen


## Registers

Unnamed register = ""
	- holds text from d,c,s,x and y operations

Numbered register = "0 "1 ... "9
	- "0 holds last text yanked (y)
	- "1 holds last deleted (d) or changed (c)
	- Numbered registers shift with each d or c

Named register = "a "b ... "z
	- 26 of them for each letter
	- use upper case A to append to the register "a - note overwrite
	- :reg 1z - means look at register 1 and z

To repeat with registers:
[count][register]operator
or
[register][count]operator

2"ap - past from "a for the two lines

"a2p - does the same thing

use the below to match the unnamed registry to the clipbaord

```bash
	set clipboard=unnamedplus
```

## Some settings

```bash
	set hls # to see highlighted search
```

## Substitute

```bash
	:[range]s/old/new/[flags]
```
example:

:1,5s/old/new/g

g - each occurance on the line is replaced

:.,$s/old/new/g
this means from current line - '.' - to the last line '$'

% = .,$

:%s/odl/new/g

:/pattern1/,/pattern2/s/old/new/g # for pattern match

## Text Object

{operator}{a}{object}
{operator}{i}{object}

Examples:
daw = delete A Word, detele around word, or delete all word
ciw = change Inner Word

dis = delete inner sentence, das = delete all sentence

dip = delete inner paragraph, dap


ci[ = clear inner within [, change everything in the braket

## Macros

repeat a number of commands.

qa = start recording macro to registry a
:req a = see what has been recorded
@a = play the marco
@@ = replay the macro


Normalize the curser -> 0
Perform edits and operations
Position your cursor to enable easy replys -> j

:27,35 normal @a -> apply marcro to lines 27 to 35 from registry a
:.,$ -> from current line to the end of the file

viminfo file
.viminfo - can store the registers
.vimrc - can store the macros

## Visual Model
v - start visual mode
o - move to the other end of the selection mode
O - move to the other end of the line in the selection
gv - repeat the selection

shift + v - line visual mode
ctrl + v - block visual mode

commands to go with visual model:

~ - Switch case
c - Chan
d - Delete
y - Yank
r - Replace
x - Delete
I - Insert
J - Join
u - Make lowercase
U - Make uppercase
> - Shift right - check ':set shiftwidth?' to see what width you get
< - Shift left
p - paragraph

va{ - select all in the braces

:set tabstop? - width of tab
:set expandtab? - to see if you wish to expand with tab

## Options in VIM
:h option-list	 " to see what options you have available
:options  	 " to see the options as well

## My .vimrc

let @m = 'ITODO: j' 	" example of a marco that can be stored here
set number
set rnu
set clipboard=unnamedplus
set hlsearch
set history=1000 	" keep 1000 last commands
set wildmenu 		" this show menu when tab completing the commands
set ai 			" set ai to it copies indentation from the previous line
set si 			" set smart auto indentation for a given coding language
let mapleader="," 	" this let's define the leader, should come first
map <leader>w :w!<CR> 	"\ is the leader and then you can do your mapping

set ruler 			" show the cursor positiondd
set showcmd 			" show incomplete commands
set scrolloff=4 		" set few lines on the top after z - enter
set ingnorecase 		" innore case when searching
set backup 			" create a backup for the file
set bex=BackUpExtentionName
set bg=light 			" set light background
set bg=dark 			" set dark background
set hidden 			" allows you to edit multiple fills in the memory buffers

"map KEY KEYSTROKES
map <F2> iJohn Smith <CR> 123 Main Street <CR> NY <ESC>

## Buffers

vim textfile1 textfile2 "opens multiple files in buffers

:ls or :buffers "list the buffers
:b1 or :buffer1 "switch to a buffer

:b tab			" to tab through the buffers
:b ctrl+d		" list the buffers for you
:bnext :bin :bn :bp	" rote through the buffers
:bn :bp :bl :bf		" :bnext :bprevious :blast :bfirst
ctrl + shift + 6 	" go to the previous buffer - means ctrl + ^
:bn! 			" switch without saving the changes - it stays in the memorey
:qall!			" quits all changes without saving for all buffers
:wall			" saves all buffers
:badd filename 		" add files to the buffers
:bd filename 		" removes the specific buffer
:bd :bdelete		" close the current buffer
:1,3bd			" removes buffers between 1 and 3 inclusively
:%bd			" delete all buffers
:bufdo set nu 		" sets numbers in all buffers
:bufdo %s/#/@/g | w	" change # to @ for all line - | w means write the file as you go along
:bufdo %s/@/a/g		" works if hidden option is on
:E			" open File Expolerer window - close it with :bd

## Windows

:sp :split		" horizontal split
ctrl + ws		" same as :sp
:sp filename		" creates a new buffer for the open window
:vp :vsplit		" vertical split - the same buffer
ctrl + wv		" same as :vp
:vp filename		" new buffer for a file
ctrl + wq		" close the window
:only :on		" close all windows but not only the one you have selected
ctrl + wo		" same as :on
ctrl + hjkl/arrow	" navigate
ctrl + w+		" increase the height of the window
ctrl + w-		" decrease the height of the window
ctrl + w>		" to increase the width
ctrl + w<		" to decrease the width
ctrl + w +underscore	" to max the height of the window
ctrl + w|		" to max the width
ctrl + w=		" make the windows the same size
ctrl + wr		" rotes the windows
ctrl + wR		" rotes in the opposite direction
ctrl + wHJKL		" moves the windows
:ba :ball		" open all buffers to windows
:windo			" executes commands in all windows
:h ctrl-w		" to get the list of the commands

Can create mapping
map <C-h> <C-w>h	map <leader>h <C-w>h
map <C-j> <C-w>j	map <leader>j <C-w>j
map <C-k> <C-w>k	map <leader>k <C-w>k
map <C-l> <C-w>l	map <leader>l <C-w>l




